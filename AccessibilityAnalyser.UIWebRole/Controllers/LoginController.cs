﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Accessibility.ServicesLayer;
using AccessibilityAnalyser.Web.UI.ViewModels;
using System.ServiceModel;
using System.Configuration;
using System.Reflection;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class LoginController : Controller
    {
        LoginService loginService = new LoginService();        
       
        public ActionResult Index()
        {
            return View("Index", new LoginViewModel());
        }

        [HttpPost]
        public ActionResult UserAccountLogin(LoginViewModel loginViewModel)
        {
            try
            {
                List<LoginBO> loginObj = loginService.GetUsers();

                foreach (LoginBO login in loginObj)
                {
                    if ((System.String.Compare(loginViewModel.AccountUserName, login.UserName, true) == 0) && loginViewModel.AccountUserPassword == login.Password)
                    {
                        Session["Username"] = loginViewModel.AccountUserName;
                        return new EmptyResult();
                    }
                }
            }
            catch (Exception ex)
            {   
                LogException.CatchException(ex, "LoginController", "UserAccountLogin");
                this.ViewBag.SuccessMsg = ex.Message;
            }

            return this.Json(new { success = false, message = "The  preference(s) has been already shared for the user: " });

        }

        [HttpPost]
        public ActionResult Logout()
        {
            System.Web.HttpContext.Current.Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}
