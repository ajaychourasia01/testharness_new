﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessibilityAnalyser.RuleEngine;
using Accessibility.ServicesLayer;
using System.Configuration;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class RulesController : Controller
    {
        /// <summary>
        /// RulesDetails Action page.
        /// </summary>
        /// <returns></returns>
        
        List<RuleDetails> getRuleDetails = new List<RuleDetails>();

        RuleServices ruleService = new RuleServices();
       
        /// <summary>
        /// Get all rule details based on group id
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public ActionResult _RuleListTreeView(FormCollection collection)
        {
            try
            {
                string groupId = collection["array"];
                getRuleDetails.AddRange(ruleService.GetRuleDetails(groupId));

                this.ViewData["RuleDetails"] = getRuleDetails;
            }
            catch (Exception ex)
            {   
                LogException.CatchException(ex, "RulesController", "_RuleListTreeView");
            }

            return PartialView(getRuleDetails);
        }

        /// <summary>
        /// Get all groups and rule details
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                getRuleDetails.AddRange(ruleService.GetAllRuleDetails());

                this.ViewData["RuleDetails"] = getRuleDetails;
                IEnumerable<StandardBO> standardList = ruleService.GetAllGroups();
                this.ViewData["RuleStandard"] = standardList;
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RulesController", "Index");
            }

            return View();
        }


    }
}
