﻿var Utils = function () {
    this.OpenDialog = function (objId, iWidth, iHeight) {
        $("#" + objId).dialog({
            width: iWidth,
            height: iHeight,
            autoOpen: true,
            hide: {
                effect: "drop",
                direction: "down"
            },
            modal: true
        });
    }

    this.LoadingDialog = function (objId, iWidth, iHeight) {
        $("#" + objId).dialog({
            width: iWidth,
            height: iHeight,
            autoOpen: true,
            hide: {
                effect: "drop",
                direction: "down"
            },
            modal: true,
            close: function (ev, ui) { $(this).remove(); }
        });
    }

    this.OpenOKDialog = function (objId, strMessage, iWidth, iHeight) {
        var objectId = "#" + objId;
        $(objectId).html('');
        $(objectId).append('<font size="2">' + strMessage + '</font><center><div class="popup_button_ok" id="OkClick">  Ok</div></center>');
        this.OpenDialog(objId, iWidth, iHeight);

        $("#OkClick").click(function () {
            $(objectId).dialog("close");
        });
    }

    this.getUrlQueryString = function (strUrl, QueryParameter) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + QueryParameter + "=([^&#]*)"; //matching the letters present after the QueryParameter
        var regex = new RegExp(regexS);
        var results = regex.exec(strUrl);   //find in url
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

