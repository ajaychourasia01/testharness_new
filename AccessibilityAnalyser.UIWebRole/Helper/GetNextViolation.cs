﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Accessibility.ServicesLayer;
using AccessBusEnt = AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.Helper
{
    public class GetNextViolation
    {
        List<AccessBusEnt.ViolationBO> violations;
        int totalCount;
        static int currentItem;
        bool lastItem;
        bool firstItem;

        public GetNextViolation(List<AccessBusEnt.ViolationBO> _violations, int _currentItem)
        {
            violations = _violations;
            totalCount = _violations.Count;
            currentItem = _currentItem;
            if (_violations.Count <= 1)
            {
                lastItem = true;
                firstItem = true;
            }
            else if (currentItem > 0 && currentItem < totalCount-1)
            {
                lastItem = false;
                firstItem = false;
            }
            else if (currentItem == 0)
            {
                lastItem = false;
                firstItem = true;
            }
            else if (currentItem == totalCount-1)
            {
                lastItem = true;
                firstItem = false;
            }
        }

        public bool IsLastItem
        {
            get { return lastItem; }

            set { lastItem = value; }
        }

        public bool IsFirstItem
        {
            get { return firstItem; }

            set { firstItem = value; }
        }

        public AccessBusEnt.ViolationBO getNext()
        {
            AccessBusEnt.ViolationBO violation = new AccessBusEnt.ViolationBO();

            currentItem++;

            if (currentItem == totalCount - 1)
            {
                violation = violations[currentItem];
                IsLastItem = true;
            }
            else
            {
                violation = violations[currentItem];
                IsLastItem = false;
            }

            if (currentItem > 0)
            {
                IsFirstItem = false;
            }
            else
            {
                IsFirstItem = true;
            }
            return violation;
        }

        public AccessBusEnt.ViolationBO getPrevious()
        {
            AccessBusEnt.ViolationBO violation = new AccessBusEnt.ViolationBO();

            currentItem--;

            if (currentItem == 0)
            {
                violation = violations[currentItem];
                IsFirstItem = true;
            }
            else
            {
                violation = violations[currentItem];
                IsFirstItem = false;
            }
            if (currentItem == totalCount - 1)
            {
                IsLastItem = true;
            }
            else
            {
                IsLastItem = false;
            }

            return violation;
        }
    }
}