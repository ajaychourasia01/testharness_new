﻿//-----------------------------------------------------------------------
// <copyright file="WrappedJsonResult.cs" company="Microsoft">
//     Copyright (c) Microsoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace AccessibilityAnalyser.Web.UI.Helper
{
    using System.Web.Mvc;

    /// <summary>
    /// Defines Wrapped JSon Result
    /// </summary>
    public class WrappedJsonResult : JsonResult
    {
        /// <summary>
        /// Executes the result based on Context
        /// </summary>
        /// <param name="context">Controller Context</param>
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Write("<html><body><textarea id=\"jsonResult\" name=\"jsonResult\">");
            base.ExecuteResult(context);
            context.HttpContext.Response.Write("</textarea></body></html>");
            context.HttpContext.Response.ContentType = "text/html";
        }
    }
}