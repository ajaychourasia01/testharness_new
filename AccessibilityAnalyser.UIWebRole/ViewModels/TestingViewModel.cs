﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Accessibility.ServicesLayer;
using AccessibilityAnalyser.RuleEngine;
using System.ComponentModel.DataAnnotations;
using AccessibilityAnalyser.Web.UI.Helper;
using AccessibilityAnalyser.BusinessEntities;


namespace AccessibilityAnalyser.Web.UI.ViewModels
{
    public class TestingViewModel
    {
        //Ajay Added
        public string RuleName { get; set; }

        public List<TestCase> testCases = new List<TestCase>();
        public List<Build> builds = new List<Build>();
        public List<AccessibilityAnalyser.BusinessEntities.ViolationBO> violations = new List<AccessibilityAnalyser.BusinessEntities.ViolationBO>();
        public AccessibilityAnalyser.BusinessEntities.ViolationBO violation = new AccessibilityAnalyser.BusinessEntities.ViolationBO();
        public List<AccessibilityAnalyser.BusinessEntities.ClientBO> clients = new List<AccessibilityAnalyser.BusinessEntities.ClientBO>();
        public string clientName { get; set; }
        public string siteName { get; set; }
        public int ruleDetailId { get; set; }
        public int violationId { get; set; }
        public string violationDescription { get; set; }
        public string violationRecommendation { get; set; }
        public string multipleChoice { get; set; }
        public int ViolationCount { get; set; }

        public List<TestCase> testCasesPaged = new List<TestCase>();
        public Paging paging { get; set; }
        public GetNextViolation getNextViolation { get; set; }

        public string selectedRules { get; set; }

        public string getviolation { get; set; }
        public string ViolationType { get; set; }
        public string ViolationTypeSelected { get; set; }
        [Required(ErrorMessage = "Please choose a run")]
        public int runId { get; set; }
        [Required(ErrorMessage = "Please choose a build")]
        public int buildId { get; set; }
        public bool IsAutomated { get; set; }
        public string violationName { get; set; }
        public string violationSourceElement { get; set; }
        public int violationSourceLineNumber { get; set; }
        public string SearchViolation { get; set; }
        public AccessibilityAnalyser.BusinessEntities.Violations Violations { get; set; }

        // For Pagination
        public int TestCaseId { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public int TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int TotalPage { get; set; }
        public int DeclinedCount { get; set; }
        public int AcceptedCount { get; set; }
        public int TotalFilteredRecords { get; set; }
        public string testcaseName { get; set; }
        

    }
}