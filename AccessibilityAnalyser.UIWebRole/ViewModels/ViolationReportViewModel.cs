﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccessibilityAnalyser.Web.UI.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ViolationReportViewModel
    {
        public int ViolationID { get; set; }
        public string ViolationName { get; set; }
        public string ViolationDescription { get; set; }
        public int TRMappingID { get; set; }
        public string SourceURL { get; set; }
        public string SourceElement { get; set; }
        public int SourceLineNumber { get; set; }
        public string ErrorDescription { get; set; }

        public bool IsActive
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }


        //public TestCaseRuleMap TestCaseRuleMap
        //{
        //    get;
        //    set;

        //}
    }

    //public class TestCaseRuleMap
    //{
    //    public int TRMappingID { get; set; }
    //    public int RuleDetailID { get; set; }
    //    public int TestCaseID { get; set; }

    //    public int IsActive
    //    {
    //        get;
    //        set;
    //    }

    //    public string CreateBy
    //    {
    //        get;
    //        set;
    //    }

    //    public DateTime CreateDate
    //    {
    //        get;
    //        set;
    //    }

    //    public string UpdateBy
    //    {
    //        get;
    //        set;
    //    }

    //    public DateTime UpdateDate
    //    {
    //        get;
    //        set;
    //    }
    //}


    public class ViolationViewModel
    {
        public IEnumerable<ViolationReportViewModel> Elements { get; set; }
    }
}