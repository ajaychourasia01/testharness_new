﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Common
{
    ///// <summary>
    ///// Rules Hierarchies - Entity Names
    ///// </summary>
    //public enum EnumDBEntityName
    //{
    //    Client,
    //    Site,
    //    Build,
    //    TestCase,
    //    Standards,
    //    Rules,
    //    RuleDetails,
    //    TestCaseRuleMappings,
    //    Violation
    //}

    /// <summary>
    /// Data stores
    /// </summary>
    public enum DataAdapterEnum
    {
        AZURETABLE,
        SQLAZURE,
        NOSQL,
        FLATFILE,
        XML,
        SQLSERVER
    }

    public enum RuleDetailTypeEnum
    {
        ISAUTOMATED,
        MANUAL
    }

    public static class StatusValues
    {
        public const string Failed = "F";
        public const string Passed = "P";
        public const string Manual = "M";

    }

    }
