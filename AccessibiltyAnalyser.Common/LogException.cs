﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AccessibilityAnalyser.Common
{
    public class LogException
    {
        static RA.EnterpriseLibrary.Logging.IRALogger serviceLogger = RA.EnterpriseLibrary.Logging.InfrastructureParts.Instance.Logger;

        public static void CatchException(Exception ex, string className, string methodName)
        {
            string logFileType = ConfigurationManager.AppSettings["LogFileType"];
            string innerExDetails = ex.Message;
            serviceLogger.Log(string.Format("{0} : {1}() {2}", className, methodName, innerExDetails), logFileType);
        }

        public static void CatchExceptionTestService(string url)
        {
            string logFileType = ConfigurationManager.AppSettings["LogFileType"];
            serviceLogger.Log(string.Format("Could not be able to exceute for {0} url", url), logFileType);
        }

    }
}
