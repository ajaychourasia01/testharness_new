﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

//This is third-party assembly used to extract the HTML content of given URL.
using HtmlAgilityPack;

namespace AccessibilityAnalyser.Common
{
    public class HTMLExtractor
    {
        public static string AuthenticationToken { get; set; }

        #region ::: Public Functions :::
        
        /// <summary>
        /// Extracts the Html content for the given url.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HtmlDocument GetHtmlContent(string url)
        {
            try
            {
                HtmlWeb container = new HtmlWeb();
                container.UseCookies = true;
                container.PreRequest = new HtmlWeb.PreRequestHandler(OnPreRequest);                
                HtmlDocument doc = container.Load(url);
                return doc;
            }
            catch(HtmlWebException ex)
            {
                return null;
            }
        }

        private static bool OnPreRequest(HttpWebRequest request)
        {
            //AddCookiesTo(request);               // Add cookies that were saved from previous requests
            //if (_isPost) AddPostDataTo(request); // We only need to add post data on a POST request
            request.KeepAlive = false;
            request.Method = "GET";
            request.Headers.Add("webauthtoken",  AuthenticationToken);
            request.ContentType = "text/html";
            request.AllowAutoRedirect = true;

            return true;
        }

        /// <summary>
        /// Extracts the HTMLNodes from raw HTML Content for validation.
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <param name="elementToBeValidated"></param>
        /// <returns></returns>
        public static List<HtmlAgilityPack.HtmlNode> GetHtmlContentElements(HtmlDocument htmlContent, string elementToBeValidated)
        {
            List<HtmlAgilityPack.HtmlNode> sourceElements = new List<HtmlAgilityPack.HtmlNode>();
           
            //Extracts the nodes of specific type.
            HtmlAgilityPack.HtmlNodeCollection nodes = htmlContent.DocumentNode.SelectNodes(elementToBeValidated.ToLower());

            if (nodes != null)
            {
                foreach (HtmlAgilityPack.HtmlNode node in nodes)
                {
                    sourceElements.Add(node);
                }
            }

            return sourceElements;
        }

        #endregion
    }
}
