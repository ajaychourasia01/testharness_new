﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Resources;

namespace AccessibilityAnalyser.Common
{
    public class JSParser
    {
        static ResourceManager resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);
        
        /// <summary>
        /// Checks if the controls is disabled
        /// </summary>
        /// <param name="scriptNodes"></param>
        /// <param name="idValue"></param>
        /// <returns></returns>
        public static bool CheckFocusDisability(List<HtmlNode> scriptNodes, string idValue, Dictionary<string, string> classProperties)
        {
            bool IsDisabled = false;
            List<string> lstFunctions = new List<string>();
            string innerText = string.Empty;            


            foreach (HtmlNode nodes in scriptNodes)
            {
                if (nodes.InnerText.Contains(idValue))
                {
                    IsDisabled = CheckDisability(nodes.InnerText.ToLower(), idValue);
                    if (IsDisabled)
                    {
                        break;
                    }
                    IsDisabled = CheckOpacity(nodes.InnerText.ToLower(), idValue, classProperties);
                    if (IsDisabled)
                    {
                        break;
                    }
                }
            }
            return IsDisabled;
        }

        /// <summary>
        /// Checks if the Control is disabled of not
        /// </summary>
        /// <param name="ScriptText"></param>
        /// <param name="idValue"></param>
        /// <returns></returns>
        private static bool CheckDisability(string ScriptText, string idValue)
        {
            bool IsAvailable = false;

            //Splits the string based on /n 
            string[] lines = ScriptText.Split(new string[] { "\n" }, StringSplitOptions.None);

            //Checks if disabled property is defined
            var lst = (from id in lines.Where(x => x.Contains(idValue))
                       where id.Contains(resourceManager.GetString("Disabled"))
                       select id).ToList();

            //Checks for true or disabled value for the disabled property
            lst = (from id in lst
                   where (id.Contains("true") || id.Contains("'disabled'"))
                   select id).ToList();

            IsAvailable = lst.Count > 0;

            return IsAvailable;
        }

        /// <summary>
        /// Check if opacity id defined in the css
        /// </summary>
        /// <param name="ScriptText"></param>
        /// <param name="idValue"></param>
        /// <returns></returns>
        private static bool CheckOpacity(string ScriptText, string idValue, Dictionary<string, string> classProperties)
        {
            bool IsAvailable = false;            
            string regexString = @"\(([^)]*)\)";
            string cssClass = string.Empty;

            //Splits the string based on /n 
            string[] lines = ScriptText.Split(new string[] { "\n" }, StringSplitOptions.None);

            //Checks if disabled property is defined
            var lst = (from id in lines.Where(x => x.Contains(idValue))
                       where id.Contains(resourceManager.GetString("AddClass"))
                       select id.Substring(id.IndexOf(resourceManager.GetString("AddClass")))).ToList();


            foreach (string item in lst)
            {
                cssClass = Regex.Match(item, regexString).Groups[1].Value.Replace("'", string.Empty);
                if (!string.IsNullOrEmpty(cssClass))
                {
                    var opacityCount = (from properties in classProperties.Where(x => x.Key == resourceManager.GetString("Dot") + cssClass)
                                        let key = GetStyleProperties(properties.Value).SingleOrDefault(x => x.Key.ToLower().Contains(resourceManager.GetString("Opacity")))
                                        where (!string.IsNullOrEmpty(key.Value) && key.Value == "0")
                                        select properties).ToList().Count;

                    IsAvailable = opacityCount > 0;
                }
            }

            return IsAvailable;
        }
        

        //Gets the css property and value in form of a dictionary
        public static Dictionary<string, string> GetStyleProperties(string style)
        {
            Dictionary<string, string> styleProperties = new Dictionary<string, string>();
            styleProperties = style.Split(';').Select(part => part.Split(':')).Where(part => part.Length == 2).GroupBy(pair => pair[0].ToString()).Select(group => group.First()).ToDictionary(sp => sp[0], sp => sp[1]);
            return styleProperties;
        }

        /// <summary>
        /// Check jQuery Events
        /// </summary>
        /// <param name="scriptNodes"></param>
        /// <param name="idValue"></param>
        /// <returns>Bool</returns>
        public static bool CheckEvents(List<HtmlNode> scriptNodes, string idValue)
        {
            string strLoad = "$(document).ready(function()";

            //string ID = "$" + "# " + idValue + " ";
            int len = strLoad.Length;
            bool flag = false;
            List<string> lstFunctions = new List<string>();
            string innerText = string.Empty;
            //string pattern = Regex.Escape("{") + "(.*?)}"; 
            try
            {


                foreach (HtmlNode nodes in scriptNodes)
                {
                    if (nodes.InnerText.Contains(strLoad))
                    {
                        int start = nodes.InnerText.ToLower().Trim().IndexOf(strLoad);
                        int end = nodes.InnerText.ToString().Trim().Length - (start + len);
                        var innertext = nodes.InnerText.ToString().Substring(start + len, end);
                        var lst_InnerText = innertext.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        int open = 0;
                        int close = 0;
                        StringBuilder str = new StringBuilder();
                        string Finalstr = string.Empty;
                        foreach (var item in lst_InnerText)
                        {
                            if (item.Trim().Contains("{"))
                            {
                                open++;
                            }
                            if (item.Trim().Contains("}"))
                            {
                                close++;
                            }
                            str.Append("\r" + item.Trim());
                            if (open == close && (open > 0 && close > 0))
                            {
                                open = close = 0;
                                Finalstr = str.ToString();

                                break;
                            }
                        }
                        //now split if by {
                        StringBuilder str1 = new StringBuilder();

                        var lst_str = ReplaceFirst(Finalstr, "{", "");
                        lst_str = ReplaceLastOccurrence(lst_str, "});", "");

                        var Each = lst_str.Split('\r');
                        List<string> lst = new List<string>();
                        //int count = 0;
                        foreach (var item in Each)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (item.Trim().Contains("{"))
                                {
                                    open++;
                                }
                                if (item.Trim().Contains("}"))
                                {
                                    close++;
                                }
                                str1.Append(item.Trim());
                                if (open == close && (open > 0 && close > 0))
                                {
                                    lst.Add(str1.ToString());
                                    str1.Clear();
                                    //lst.AddRange(str1.ToString());
                                }
                            }
                        }

                        var final = (from x in lst
                                     where x.Contains(idValue) && x.Contains("preventDefault")
                                     select x).ToList();
                        if (final.Count > 0)
                        {
                            flag = true;
                            break;
                        }

                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasKeyBoardInterface");
            }
            return flag;
        }

        /// <summary>
        /// Finds and replace first occrance for a char
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="Find"></param>
        /// <param name="Replace"></param>
        /// <returns>string</returns>
        private static string ReplaceFirst(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;

        }

        /// <summary>
        /// Finds and replace last occrance for a char
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="Find"></param>
        /// <param name="Replace"></param>
        /// <returns>string</returns>
        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.LastIndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        /// <summary>
        /// Check Javascript Events
        /// </summary>
        /// <param name="scriptNodes"></param>
        /// <param name="idValue"></param>
        /// <param name="eventName"></param>
        /// <returns>Bool</returns>
        public static bool CheckEventsName(List<HtmlNode> scriptNodes, string idValue, string eventName)
        {
            bool flag = false;
            //string funcName = "function" + " " + eventName;
            string funcName = Regex.Replace("function" + " " + eventName, @"[*._;]", string.Empty);
            int len = funcName.Length;
            List<string> lstFunctions = new List<string>();
            string innerText = string.Empty;


            foreach (HtmlNode nodes in scriptNodes)
            {
                int open = 0;
                int close = 0;
                string Finalstr = string.Empty;
                if (nodes.InnerText.Contains(idValue) && nodes.InnerText.Contains(funcName))
                {
                    int start = nodes.InnerText.ToLower().Trim().IndexOf(funcName);
                    int end = nodes.InnerText.ToString().Trim().Length - (start + len);

                    var innertext = nodes.InnerText.ToString().Substring(start + len, end);
                    var lst_InnerText = innertext.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                    //now split if by {
                    StringBuilder str = new StringBuilder();

                    foreach (var item in lst_InnerText)
                    {
                        if (item.Trim().Contains("{"))
                        {
                            open++;
                        }
                        if (item.Trim().Contains("}"))
                        {
                            close++;
                        }
                        str.Append(item.Trim());
                        if (open == close && (open > 0 && close > 0))
                        {
                            open = close = 0;
                            Finalstr = str.ToString();
                            break;
                        }
                    }
                }

                if (Finalstr.Contains("return false"))
                {
                    flag = true;
                }
            }
            return flag;
        }
    }
}
