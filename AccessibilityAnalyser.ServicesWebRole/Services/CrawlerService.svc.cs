﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using System.Threading.Tasks;
using System.Threading;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Web.Services.DataContracts;
using AccessibilityAnalyser.Web.Services.ServiceContracts;

namespace AccessibilityAnalyser.Web.Services
{
   
    public class CrawlerService : ICrawlerService
    {
        ServiceFaultData serviceFault = null;
      
        public List<string> GetUrlList(string SiteUrl, int depthLevel,string allowExternal)
        {
            List<string> urlList = new List<string>();
            Crawling crawlObj = new Crawling(); 

            try
            {
                urlList = crawlObj.GetUrlList(SiteUrl, depthLevel, allowExternal);
                
            }
            catch (TimeoutException timeOutEx)
            {
                string innerExDetails = timeOutEx.Message;
                //serviceLogger.Log("Timeout Exception raised from CrawlService " + innerExDetails, "SLFile");
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "CrawlService Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }
            catch (CommunicationException communicationEx)
            {
                string innerExDetails = communicationEx.Message;
                //serviceLogger.Log("Communication Exception raised from CrawlService " + innerExDetails, "SLFile");
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "CrawlService Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }
            catch (Exception generalEx)
            {
                string innerExDetails = generalEx.Message;
                // serviceLogger.Log("Exception raised from CrawlService " + innerExDetails, "SLFile");
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "CrawlService Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }

            return urlList;
        }
    }
}
