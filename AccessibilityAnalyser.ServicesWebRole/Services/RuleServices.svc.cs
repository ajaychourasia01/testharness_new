﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RuleServices" in code, svc and config file together.
    public class RuleServices : IRuleServices
    {
        public List<RuleDetails> GetAllRuleDetails()
        {
            return Rules.GetAllRuleDetails();
        }

        public IEnumerable<StandardBO> GetAllGroups()
        {
            return Rules.GetAllGroups();
        }

        public List<RuleDetails> GetRuleDetails(string groupID)
        {
            return Rules.GetRuleDetails(groupID);
        }
    }
}
