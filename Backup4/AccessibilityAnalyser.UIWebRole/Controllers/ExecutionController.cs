﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Accessibility.ServicesLayer;
using AccessibilityAnalyser.Web.UI.ViewModels;
using System.ServiceModel;
using System.Configuration;
using AccessibilityAnalyser.Common;
using System.Diagnostics;
using AccessibilityAnalyser.RuleEngine;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text;
using System.ServiceProcess;
using AccessibilityAnalyser.BusinessEntities;


namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class ExecutionController : Controller
    {
        int siteId;
        int buildId;
        int clientId;

        //List<ClientBO> clientList = new List<ClientBO>();

        ExecutionServices executionservice = new ExecutionServices();
        TestServices testClient = new TestServices();
        CrawlerService crawlerClient = new CrawlerService();

        ServiceController serviceObj = new ServiceController("CrawlExecutionSequencer");

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Serch Client 
        /// </summary>
        /// <param name="term">string</param>
        /// <returns>ActionResult</returns>
        public ActionResult QuickSearchClient(string term)
        {
            var clients = executionservice.GetClientList(term)
            .Take(10)
            .OrderBy(client => client.ClientName)
            .Select(client => new { label = client.ClientName });

            return Json(clients, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Serach URL Name
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="clientName">string</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult QuickSearchUrlName(string searchText, string clientName)
        {
            if (!string.IsNullOrEmpty(clientName))
            {
                clientId = executionservice.GetClientID(clientName);
                var clients = executionservice.GetSiteList(searchText, clientId)
                    .Take(10)
                    .OrderBy(client => client.SiteName)
                    .Select(client => new { label = client.SiteName });

                return Json(clients, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Search Build Name
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="urlName">string</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult QuickSearchBuildName(string searchText, string urlName, string clientName)
        {
            if (!string.IsNullOrEmpty(urlName))
            {
                clientId = executionservice.GetClientID(clientName);
                siteId = executionservice.GetSiteID(urlName, clientId);
                var clients = executionservice.GetBuildList(searchText, siteId)
                .Take(10)
                .OrderBy(client => client.BuildName)
                .Select(client => new { label = client.BuildName });
                return Json(clients, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Search Run Name
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="buildName">string</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult QuickSearchRunName(string searchText, string buildName, string urlName, string clientName)
        {
            if (!string.IsNullOrEmpty(buildName))
            {
                clientId = executionservice.GetClientID(clientName);
                siteId = executionservice.GetSiteID(urlName, clientId);
                buildId = executionservice.GetBuildID(buildName, siteId);
                var clients = executionservice.GetRunList(searchText, buildId)
                .Take(10)
                .OrderBy(client => client.RunName)
                .Select(client => new { label = client.RunName });

                return Json(clients, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// After providing alll the information for Executuion Actionm
        /// </summary>
        /// <param name="collection">FormCollection</param>
        /// <returns>ActionResult</returns>;
        [HttpPost]
        public ActionResult _Crawl(FormCollection collection)
        {
            string siteName = collection["UrlName"].ToString();
            siteId = executionservice.GetSiteID(siteName, clientId);

            string depth = collection["depthLevel"].ToString();
            int depthLevel = 0;
            string authority = collection["Authority"].ToString();

            if (!depth.Equals("All"))
                depthLevel = Convert.ToInt32(depth);
            string crawlType = collection["CrawlType"];

            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                this.ViewData["ActionStatus"] = "";
                this.ViewData["URLList"] = crawlerClient.GetUrlList(siteName, depthLevel, authority, crawlType);
                sw.Stop();
                TimeSpan elapsedTime = sw.Elapsed;
                StringBuilder sbUrls = new StringBuilder();
                int urlCount = 0;

                foreach (string url in ViewData["URLList"] as List<string>)
                {
                    sbUrls.Append(url + "~");
                }

                urlCount = (ViewData["URLList"] as List<string>).Count;
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionController", "_Crawl");
            }

            return PartialView();
        }

        [HttpPost]
        public ActionResult Validate_Data(FormCollection collection)
        {
            bool DupData = true;
            try
            {
                clientId = executionservice.GetClientID(collection["ClientName"].ToString());
                siteId = executionservice.GetSiteID(collection["UrlName"].ToString(), clientId);
                buildId = executionservice.GetBuildID(collection["BuildName"].ToString(), siteId);
                if (clientId != 0 && siteId != 0 && buildId != 0)
                {
                    DupData = true;
                }
                else
                {
                    DupData = false;
                }
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionController", "Validate_Data");
            }
            return Json(new { Duplicate = DupData.ToString() });
        }

        /// <summary>
        /// Sets Excel cell value
        /// </summary>
        /// <param name="targetSheet">Contains the excel sheet object in which the cell value is going to be set </param>
        /// <param name="Cell">Cell index</param>
        /// <param name="Value">Cell value to set</param>
        private static void SetCellValue(Excel.Worksheet targetSheet, string Cell, object Value)
        {
            targetSheet.get_Range(Cell, Cell).set_Value(Excel.XlRangeValueDataType.xlRangeValueDefault, Value);
        }

        /// <summary>
        /// Adds all crawled urls to excel file
        /// </summary>
        /// <param name="excelPath">Excel file path</param>
        /// <param name="clientName">Client name</param>
        /// <param name="buildName">Build name</param>
        /// <param name="runName">Run name</param>
        /// <param name="urlList">List of crawled urls</param>
        private void AddUrlsToExcel(string excelPath, string clientName, string buildName, string runName, string siteName, List<string> urlList)
        {
            Excel.Application excelApplication = new Excel.Application();            
            Excel.Workbook newWorkbook = null;

            try
            {
                object paramMissing = Type.Missing;
                newWorkbook = excelApplication.Workbooks.Add(paramMissing);

                Excel.Worksheet activeWorkSheet = null;
                activeWorkSheet = (Excel.Worksheet)newWorkbook.Worksheets[1];
                char c = Convert.ToChar('A');

                activeWorkSheet.get_Range("A1", "B1").Merge();
                activeWorkSheet.get_Range("A1", "B1").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                activeWorkSheet.get_Range("A1", "B1").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
                activeWorkSheet.get_Range("A1", "B1").Font.Size = "16";
                activeWorkSheet.get_Range("A1", "B1").Font.Bold = true;
                SetCellValue(activeWorkSheet, "A1", "Crawled URLs List");

                SetCellValue(activeWorkSheet, "A2", "Client Name");
                SetCellValue(activeWorkSheet, "B2", clientName);
                SetCellValue(activeWorkSheet, "A3", "Build Name");
                SetCellValue(activeWorkSheet, "B3", buildName);
                SetCellValue(activeWorkSheet, "A4", "Run Name");
                SetCellValue(activeWorkSheet, "B4", runName);
                SetCellValue(activeWorkSheet, "A5", "Url Name");
                SetCellValue(activeWorkSheet, "B5", siteName);
                activeWorkSheet.get_Range("A2", "A5").EntireColumn.AutoFit();
                activeWorkSheet.get_Range("A2", "A5").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGreen);
                activeWorkSheet.get_Range("B2", "B5").EntireColumn.AutoFit();
                activeWorkSheet.get_Range("B2", "B5").Font.Bold = true;

                activeWorkSheet.get_Range("A6", "A6").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                activeWorkSheet.get_Range("A6", "A6").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                activeWorkSheet.get_Range("A6", "A6").Font.Size = "12";
                activeWorkSheet.get_Range("A6", "A6").Font.Bold = true;
                SetCellValue(activeWorkSheet, "A6", "S.No");

                activeWorkSheet.get_Range("B6", "B6").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                activeWorkSheet.get_Range("B6", "B6").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                activeWorkSheet.get_Range("B6", "B6").Font.Size = "14";
                activeWorkSheet.get_Range("B6", "B6").ColumnWidth = "150";
                SetCellValue(activeWorkSheet, "B6", "URL");

                int counter = 7;
                foreach (string url in urlList)
                {
                    SetCellValue(activeWorkSheet, "A" + counter.ToString(), counter - 6);
                    SetCellValue(activeWorkSheet, "B" + counter.ToString(), url);
                    ++counter;
                }

                newWorkbook.SaveAs(excelPath, paramMissing, paramMissing,
        paramMissing, paramMissing, paramMissing,
        Excel.XlSaveAsAccessMode.xlNoChange, paramMissing, paramMissing,
        paramMissing, paramMissing, paramMissing);                

                // Close and release the Workbook object.
                if (newWorkbook != null)
                {
                    newWorkbook.Close(false, paramMissing, paramMissing);
                    newWorkbook = null;
                }

                // Quit Excel and release the ApplicationClass object.
                if (excelApplication != null)
                {
                    excelApplication.Quit();
                    excelApplication = null;
                }

            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionController", "AddUrlsToExcel");
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Generates unique file name for the excel file which will be downloaded by the user.
        /// </summary>
        /// <returns>Unique file name</returns>
        private string GetUniqueFileName()
        {
            if (Session["Username"] != null)
            {
                return "CrawledUrlList_" + Session["Username"].ToString() + "_" + DateTime.Now.ToFileTime() + ".xlsx";
            }

            return "CrawledUrlList_" + DateTime.Now.ToFileTime() + ".xlsx";
        }

        [HttpPost]
        public ActionResult ImporttoExcel(FormCollection collection)
        {
            string fileName = string.Empty;
            string clientName = string.Empty;
            string buildName = string.Empty;
            string runName = string.Empty;
            string siteName = string.Empty;
            string filePath = string.Empty;            
            List<string> crawledURLList = new List<string>();

            clientName = collection["ClientName"].ToString();
            buildName = collection["BuildName"].ToString();
            runName = collection["RunName"].ToString();
            siteName = collection["SiteName"].ToString();

            string crawledURLArray = collection["CrawledURLArray"];

            crawledURLList = crawledURLArray.Substring(0, crawledURLArray.Length - 1).Split("~".ToCharArray()).ToList();
            fileName = GetUniqueFileName();
            this.ViewData["FileName"] = fileName;
            filePath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Downloads"), fileName);

            AddUrlsToExcel(filePath, clientName, buildName, runName, siteName,crawledURLList);

            return this.Json(new { FileName = fileName });
        }

        /// <summary>
        /// Execute the urls
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Execution(FormCollection collection)
        {
                    

            int violationPerBatch = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ViolPerBatch"].ToString());
            string selectedURLArray = collection["SelectedURLArray"];

            string clientName = collection["ClientName"].ToString();
            clientId = executionservice.GetClientID(clientName);

            string siteName = collection["UrlName"].ToString();
            siteId = executionservice.GetSiteID(siteName, clientId);

            string buildName = collection["BuildName"].ToString();
            buildId = executionservice.GetBuildID(buildName, siteId);

            string runName = collection["RunName"].ToString();
            int runId = executionservice.GetRunID(runName, buildId, clientId);

            List<string> selectedURLList = selectedURLArray.Substring(0, selectedURLArray.Length - 1).Split("~".ToCharArray()).ToList();

            try
            {                
                HTMLExtractor.AuthenticationToken = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAiLCJ0eXAiOiJKV1QifQ.eyJ2ZXIiOjEsImlzcyI6InVybjp3aW5kb3dzOmxpdmVpZCIsImV4cCI6MTM2NDg4MTQyMCwidWlkIjoiMDVlY2U4OTExZjFmMzRjZmViZmQ3ODJjNGRlYjVjZjAiLCJhdWQiOiJteXdlYmNyYXdsZXIuY29tIiwidXJuOm1pY3Jvc29mdDphcHB1cmkiOiJhcHBpZDovLzAwMDAwMDAwNDQwRThDQTgiLCJ1cm46bWljcm9zb2Z0OmFwcGlkIjoiMDAwMDAwMDA0NDBFOENBOCJ9.-8pKRB4q3G9f3DyI2qQ3WAGS23b3D0t3lolebz0qg88";
                RuleValidator ruleValidator = new RuleValidator();
                ruleValidator.TestValidation(selectedURLList, clientId, buildId, runId, violationPerBatch);
            }
            catch (FaultException ex)
            {
                LogException.CatchException(ex, "ExecutionController", "Execution");
                this.ViewBag.SuccessMsg = ex.Message;
            }

            return PartialView("_Crawl");
        }

        [HttpGet]
        public ActionResult Execution()
        {
            return View();
        }        

        [AllowAnonymous]
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload()
        {
            string uploadedFileNames = string.Empty;
            if (ModelState.IsValid)
            {
                string physicalPath = HttpContext.Server.MapPath("../") + "Uploads" + "\\";

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    this.ViewData["UploadedFilePath"] = physicalPath + System.IO.Path.GetFileName(Request.Files[i].FileName);
                    Request.Files[0].SaveAs(physicalPath + System.IO.Path.GetFileName(Request.Files[i].FileName));
                    if (i == 0)
                    {
                        uploadedFileNames += Request.Files[i].FileName;
                    }
                    else
                    {
                        uploadedFileNames += "," + Request.Files[i].FileName;
                    }
                }
            }

            List<string> uploadDetails = ReadUrlsFromUploadedFile();

            string clientName = uploadDetails[0].ToString();
            clientId = executionservice.GetClientID(clientName);

            string siteName = uploadDetails[3].ToString();
            siteId = executionservice.GetSiteID(siteName, clientId);

            string buildName = uploadDetails[1].ToString();
            buildId = executionservice.GetBuildID(buildName, siteId);

            string runName = uploadDetails[2].ToString();
            int runId = executionservice.GetRunID(runName, buildId, clientId);

            DateTime requestTime = new DateTime();
            try
            {
                bool createLogSuccess = executionservice.CreateServiceLog(clientId, runId, buildId, "New", ref requestTime, this.ViewData["UploadedFilePath"].ToString());
            }
            catch (FaultException ex)
            {
                LogException.CatchException(ex, "ExecutionController", "Upload");
                this.ViewBag.SuccessMsg = ex.Message;
            }

            return View("Index");
        }

        private List<string> ReadUrlsFromUploadedFile()
        {
            string fileFullPath = this.ViewData["UploadedFilePath"].ToString();
            List<string> urlList = new List<string>();
            List<string> dataList = new List<string>();
            StringBuilder sbUrls = new StringBuilder();
            string clientName = string.Empty;
            string buildName = string.Empty;
            string runName = string.Empty;
            string urlName = string.Empty;

            if (System.IO.File.Exists(fileFullPath))
            {
                try
                {
                    Excel.Workbook workbook;
                    Excel.Worksheet NwSheet;
                    Excel.Range ShtRange;
                    Excel.Application appExl = new Excel.Application();

                    workbook = appExl.Workbooks.Open(fileFullPath, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    NwSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets.get_Item(1);

                    ShtRange = NwSheet.UsedRange; //gives the used cells in sheet

                    clientName = ShtRange.get_Range("B2", "B2").Text.ToString();
                    buildName = ShtRange.get_Range("B3", "B3").Text.ToString();
                    runName = ShtRange.get_Range("B4", "B4").Text.ToString();
                    urlName = ShtRange.get_Range("B5", "B5").Text.ToString();
                    
                    workbook.Close(false, Type.Missing, Type.Missing);
                    workbook = null;

                    appExl.Quit();
                    appExl = null;
                }
                catch (Exception ex)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    LogException.CatchException(ex, "ExecutionController", "ReadUrlsFromUploadedFile");
                }
                finally
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

            }

            this.ViewData["URLList"] = urlList;
            dataList.Add(clientName);
            dataList.Add(buildName);
            dataList.Add(runName);
            dataList.Add(urlName);
            dataList.Add(sbUrls.ToString());
            return dataList;
        }        

        public ActionResult BulkExecutionSection()
        {
            return PartialView("_BulkExecution");
        }
    }
}
