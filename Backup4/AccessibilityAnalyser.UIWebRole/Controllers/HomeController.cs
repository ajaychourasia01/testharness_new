﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessibilityAnalyser.Web.UI.ViewModels;
using System.Threading;
using Lib.Web.Mvc;
using HtmlAgilityPack;
using Accessibility.ServicesLayer;
using System.Resources;
using AccessibilityAnalyser.Common;
using System.Configuration;
using AccessibilityAnalyser.BusinessEntities;


namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class HomeController : AsyncController
    {
        TestServices testClient = null;
        ResourceManager resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);
      
        public HomeController()
        {
            testClient = new TestServices();

        }

        public ActionResult Test()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.Load(@"C:\Users\santoshkatti\Desktop\3.htm");
            GetAllContentEditableDiv(doc);

            return View();
        }

        /// <summary>
        ///  Input - use <input>, rather than a styled div
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public List<ViolationBO> GetAllContentEditableDiv(HtmlDocument doc)
        {
            HtmlNodeCollection divNodeCollection = doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag"));

            List<ViolationBO> violationList = new List<ViolationBO>();

            try
            {
                if (divNodeCollection != null)
                {
                    var divWithOnclickAttr = from i in divNodeCollection
                                             select new
                                             {
                                                 HasOnClick = i.Attributes.Contains(resourceManager.GetString("ContentEditable")),
                                                 Line = i.Line,
                                                 OuterHtml = i.OuterHtml
                                             };

                    foreach (var item in divWithOnclickAttr)
                    {
                        if (item.HasOnClick)
                        {
                            ViolationBO violationBO = new ViolationBO
                            {
                                SourceElement = item.OuterHtml,
                                SourceLineNumber = item.Line,
                                ViolationDescription = resourceManager.GetString("ContentEditableStyledDiv")
                            };

                            violationList.Add(violationBO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {   
                LogException.CatchException(ex, "HomeController", "GetAllContentEditableDiv");
            }
            return violationList;
        }

        public ActionResult Index()
        {
           
            //Progressbar intialization;
            Session["OPERATION_PROGRESS"] = 0;

            //Method to get list of latest top 2 clients
            HomeViewModel homeViewModel = new HomeViewModel();
            homeViewModel.clients = testClient.GetClients("*").ToList();
            foreach (ClientBO client in homeViewModel.clients)
            {
                homeViewModel.ViolationCount.Add(testClient.GetViolationCount(client.ClientId));
            }
            return View(homeViewModel);
        }

        /// <summary>
        /// Action for triggering long running operation
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Operation()
        {
            HttpSessionStateBase session = Session;

            //Separate thread for long runnig operation
            ThreadPool.QueueUserWorkItem(delegate
            {
                int operationProgress;
                for (operationProgress = 0; operationProgress <= 100; operationProgress = operationProgress + 2)
                {
                    session["OPERATION_PROGRESS"] = operationProgress;
                    Thread.Sleep(1000);
                }
            });

            return Json(new { progress = 0 });
        }

        /// <summary>
        /// Action for reporting operation progress
        /// </summary>
        /// <returns></returns>  
        [NoCache]
        public ActionResult OperationProgress()
        {
            int operationProgress = 0;

            if (Session["OPERATION_PROGRESS"] != null)
                operationProgress = (int)Session["OPERATION_PROGRESS"];

            return Json(new { progress = operationProgress }, JsonRequestBehavior.AllowGet);
        }


        public void AboutAsync(List<ViolationBO> violationReport)
        {

            var model = new ViolationViewModel();


            //testClient.TestValidationAsync("http:\\\\www.google.co.in", 1, 1, 1);

            ////testClient.TestValidationCompleted += ((sender, e) =>
            ////{
            ////    AsyncManager.Parameters.Add("violationReport", e.Result);
            ////}
            ////);
            //testClient.TestValidationCompleted +=new EventHandler<TestServices.TestValidationCompletedEventArgs>(testClient_TestValidationCompleted);

        }


        public ActionResult AboutCompleted(List<ViolationBO> violationReport)
        {

            var model = new ViolationViewModel();

            if (violationReport != null)
            {
                //model.Elements = from x in violationReport
                //                 select new ViolationReportViewModel
                //                     {
                //                         ViolationID = x.ViolationID,
                //                         ViolationName = x.ViolationName,
                //                         ViolationDescription = x.ErrorDescription,
                //                         SourceElement = x.SourceElement,
                //                         SourceLineNumber = x.SourceLineNumber,
                //                         SourceURL = x.SourceURL
                //                     };
            }

            ViewBag.Message = "Test Report is executing..";
            ViewBag.Model = model;
            return View("About");
        }



        public ActionResult Contact(ViolationViewModel model)
        {

            ViewBag.Message = "Your contact page.";

            return View(model);
        }
       
    }
}
