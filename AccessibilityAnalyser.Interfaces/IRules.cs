﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IRule
    {
        int RuleId { get; set; }
        string RuleName { get; set; }
        string RuleDescription { get; set; }
        int StandardId { get; set; }

        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        DateTime CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }
    }
}
