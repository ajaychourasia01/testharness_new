﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IBuild
    {
        int BuildId { get; set; }
        string BuildName { get; set; }
        string BuildDescription { get; set; }
        int SiteId { get; set; }
        
        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }
    }
}
