﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public class IViolationCount
    {
        int TestCaseId { get; set; }
        int ViolationsCount { get; set; }
    }
}
