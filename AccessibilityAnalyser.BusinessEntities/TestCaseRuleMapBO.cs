﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class TestCaseRuleMapBO : ITestCaseRuleMap
    {
        IDataAdapter entityStore = null;

        public TestCaseRuleMapBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }


        public int TRMappingId { get; set; }
        public int RuleDetailId { get; set; }
        public int TestCaseId { get; set; }

        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }
        public int Priority
        {
            get;
            set;
        }
        public bool CreateTestCaseRuleMapping()
        {
            return entityStore.CreateTestCaseMap(this);
        }
    }
}
