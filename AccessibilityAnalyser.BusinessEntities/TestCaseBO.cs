﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class TestCaseBO : ITestCase
    {
        IDataAdapter entityStore = null;

        public TestCaseBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public int TestCaseId { get; set; }
        public string TestCaseName { get; set; }
        public string TestCaseDescription { get; set; }
        public int BuildId { get; set; }
        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public bool CreateTestCase()
        {
            return entityStore.CreateTestCase(this);
        }
    }
}
