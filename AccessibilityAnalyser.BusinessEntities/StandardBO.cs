﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class StandardBO : IStandard
    {
        IDataAdapter entityStore = null;

        public StandardBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public int StandardId { get; set; }
        public string StandardTitle { get; set; }
        public string StandardDescription { get; set; }
        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public bool CreateStandard()
        {
            return entityStore.CreateStandard(this);
        }
    }
}
