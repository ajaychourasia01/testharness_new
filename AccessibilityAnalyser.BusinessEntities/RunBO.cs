﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.DataAdapter;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RunBO: IRun
   {
        IDataAdapter entityStore = null;

        public RunBO()
        {
            //entityStore = new DataStoreFactory().GetDataStore();
        }


        public int ClientId { get; set; }
        public int BuildId { get; set; }
        public int RunId { get; set; }

        public string RunName { get; set; }
        public string CreatedBy { get; set; }

        public Nullable<DateTime> CreatedDate { get; set; }
        public string UpdateBy { get; set; }

        public Nullable<DateTime> UpdateDate { get; set; }
        public bool IsActive { get; set; }
    }
}
