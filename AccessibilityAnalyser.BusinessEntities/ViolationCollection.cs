﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ViolationCollection : IEntityCollection<ViolationBO>
    {
        IDataAdapter entityStore = null;
        List<ViolationBO> violationInfo = null;
        List<ViolationCount> vioalationCount = null;
        List<TestCaseBO> testCase = null;

        public ViolationCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }


        public ViolationBO GetSingle(int ObjectId)
        {
            EntityObject violationObject = entityStore.GetViolation(ObjectId);

            ViolationBO violation = new ViolationBO();
            Helper.CopyPropertyValues(violationObject, violation);

            return violation;
        }

        public List<ViolationBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetViolations();

            violationInfo = new List<ViolationBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                ViolationBO violation = new ViolationBO();
                Helper.CopyPropertyValues(eo, violation);
                violationInfo.Add(violation);
            }

            return violationInfo;
        }

        public List<ViolationBO> GetRecords(int clientId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetViolations(clientId);

            violationInfo = new List<ViolationBO>();
            entityEnumerable.ToList().ForEach(delegate(EntityObject eo)
            {
                ViolationBO violation = new ViolationBO();
                Helper.CopyPropertyValues(eo, violation);
                violationInfo.Add(violation);

            });
            return violationInfo;
        }

        public int GetRecordCount(int clientId)
        {
            return entityStore.GetViolationCount(clientId);
        }

        public bool UpdateData(List<ViolationBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(ViolationBO entityData)
        {
            return entityStore.UpdateViolation(entityData);
        }

        public bool UpdateDataSingleForManual(ViolationBO entityData)
        {
            return entityStore.UpdateViolationForManual(entityData);
        }

        public bool CreateDataSingle(ViolationBO entityData)
        {
            return entityStore.CreateViolation(entityData);
        }

        public bool CreateViolations(IList<IViolation> entityData)
        {
            return entityStore.CreateViolations(entityData);
        }

        /// <summary>
        /// Delete single row/Object from violation table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool DeleteDataSingle(ViolationBO entityData)
        {
            return entityStore.DeleteViolation(entityData);
        }

        /// <summary>
        /// Delete a Multiple Data/row from Violation table in Database
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool DeleteDataMultiple(List<ViolationBO> entityData)
        {
            bool IsDeleted = false;
            foreach (ViolationBO ViolationItem in entityData)
            {
                DeleteDataSingle(ViolationItem);
            }
            IsDeleted = !IsDeleted;
            return IsDeleted;
        }

        /// <summary>
        /// Fetch Client, Build and Run specific Data from violation Table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public List<ViolationBO> GetSpecificRecords(int clientId, int buildId, int runId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetSpecificViolations(clientId, buildId, runId);
            violationInfo = new List<ViolationBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                ViolationBO violation = new ViolationBO();
                Helper.CopyPropertyValues(eo, violation);
                violationInfo.Add(violation);
            }
            return violationInfo;
        }

        /// <summary>
        /// Fetch Client, Build and Run specific Data from violation Table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public List<ViolationBO> GetBuildSpecificViolations(int buildId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetBuildSpecificViolations(buildId);
            violationInfo = new List<ViolationBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                ViolationBO violation = new ViolationBO();
                Helper.CopyPropertyValues(eo, violation);
                violationInfo.Add(violation);
            }
            return violationInfo;
        }

        public int GetViolationsCountPerRule(int buildId, int runId, int ruleDetailId, string typeCount, int testCaseId = 0)
        {
            return entityStore.GetViolationCountPerRule(buildId, runId, ruleDetailId, typeCount, testCaseId);
        }

        public List<ViolationBO> GetViolationList(int clientId, int buildId, int runId, int ruleDetailId, string violationType, int skip, int testCaseId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetViolationList(clientId, buildId, runId, ruleDetailId, violationType, testCaseId).Skip(skip).Take(10);
            violationInfo = new List<ViolationBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                var violation = new ViolationBO();
                Helper.CopyPropertyValues(eo, violation);
                violationInfo.Add(violation);
            }
            return violationInfo;
        }

        public int GetViolationCount(int clientID, int buildID, int runID)
        {
            return entityStore.GetViolationCount(clientID, buildID, runID);
        }

        public int GetFailedViolationCount(int clientID, int buildID, int runID)
        {
            return entityStore.GetFailedViolationCount(clientID, buildID, runID);
        }

        public List<TestCaseBO> GetTestCaseCount(int clientId, int buildId, int runId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetTestCaseCount(clientId, buildId, runId);
            testCase = new List<TestCaseBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                var violation = new TestCaseBO();
                Helper.CopyPropertyValues(eo, violation);
                testCase.Add(violation);
            }
            return testCase;
        }

        public int GetTestCaseViolationCount(int tcId,int clientId,int buildId,int runid)
        {
            return entityStore.GetTestCaseViolationCount(tcId,clientId,buildId,runid);
        }
        public int GetFailedTestCaseViolationCount(int tcId, int clientId, int buildId, int runid)
        {
            return entityStore.GetFailedTestCaseViolationCount(tcId, clientId, buildId, runid);
        }
        public List<TestCaseBO> GetClientTestCaseCount(int clientId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetClientTestCaseCount(clientId);
            testCase = new List<TestCaseBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                var violation = new TestCaseBO();
                Helper.CopyPropertyValues(eo, violation);
                testCase.Add(violation);
            }
            return testCase;
        }
        public int GetClientTestCaseViolationCount(int tcId, int clientId)
        {
            return entityStore.GetClientTestCaseViolationCount(tcId, clientId);
        }
        public int GetClientFailedTestCaseViolationCount(int clientID, int buildID, int runID)
        {
            return entityStore.GetFailedViolationCount(clientID, buildID, runID);
        }
        public int GetClientFailedTestCaseViolationCount(int tcId, int clientId)
        {
            return entityStore.GetClientFailedTestCaseViolationCount(tcId, clientId);
        }

    }
}
