﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class LoginCollection : IEntityCollection<LoginBO>
    {
        IDataAdapter entityStore = null;

        List<LoginBO> loginInfo = null;

        public LoginCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public List<LoginBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetUsers();
            loginInfo = new List<LoginBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                LoginBO login = new LoginBO();
                Helper.CopyPropertyValues(eo, login);
                loginInfo.Add(login);
            }

            return loginInfo;
        }

        public LoginBO GetSingle(int ObjectId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateData(List<LoginBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(LoginBO entityData)
        {
            throw new NotImplementedException();
        }
    }
}
