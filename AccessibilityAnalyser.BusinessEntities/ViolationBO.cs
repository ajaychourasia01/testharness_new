﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ViolationBO : IViolation
    {
        IDataAdapter entityStore = null;

        public ViolationBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }
        //Ajay Added
        public string RuleName { get; set; }
        public int ViolationId { get; set; }
        public string ViolationName { get; set; }
        public string ViolationDescription { get; set; }
        public int TRMappingId { get; set; }
        public string SourceURL { get; set; }
        public string SourceElement { get; set; }
        public int SourceLineNumber { get; set; }
        public string Recomandation { get; set; }
        public int AcceptedViolationsCount { get; set; }
        public int DeclinedViolationsCount { get; set; }
        public int violationLogId { get; set; }
        public int DupTRMappingId { get; set; }

        public bool IsActive
        {
            get;
            set;
        }


        public DateTime CreateDate
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public int RunId
        {
            get;
            set;
        }

        public int ClientId
        {
            get;
            set;
        }

        public int BuildId
        {
            get;
            set;
        }

        public bool CreateViolation()
        {
            this.CreateDate = DateTime.Now;
            this.UpdateDate = DateTime.Now;
            this.CreateBy = WindowsIdentity.GetCurrent().Name;
            this.UpdateBy = this.CreateBy = WindowsIdentity.GetCurrent().Name;
            this.IsActive = true;

            return entityStore.CreateViolation(this);
        }
    }
}
