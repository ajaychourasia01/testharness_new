﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ViolationCount : IViolationCount
    {
        public ViolationCount(int testCaseId,int violationCount)
        {
            this.TestCaseId = testCaseId;
            this.ViolationsCount = violationCount;
        }

        public int TestCaseId { get; set; }
        public int ViolationsCount { get; set; }
    }
}
