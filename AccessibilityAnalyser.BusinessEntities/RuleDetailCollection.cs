﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RuleDetailCollection : IEntityCollection<RuleDetailBO>
    {
        IDataAdapter entityStore = null;
        List<RuleDetailBO> ruleDetailInfo = null;

        public RuleDetailCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }


        public RuleDetailBO GetSingle(int ObjectId)
        {
            EntityObject ruleDetailObject = entityStore.GetRuleDetail(ObjectId);

            RuleDetailBO ruleDetail = new RuleDetailBO();
            Helper.CopyPropertyValues(ruleDetailObject, ruleDetail);

            return ruleDetail;
        }

        public List<RuleDetailBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetRuleDetails();

            ruleDetailInfo = new List<RuleDetailBO>();
            RuleDetailBO ruleDetail = null;

            entityEnumerable.ToList().ForEach(eo =>
            {

                ruleDetail = new RuleDetailBO();

                Helper.CopyPropertyValues(eo, ruleDetail);
                if (ruleDetail.IsActive == true)
                { // Condition added and Modified by Chaitanya G for populating the active rules.
                    ruleDetailInfo.Add(ruleDetail);
                }

            });

            //foreach (EntityObject eo in entityEnumerable)
            //{
            //    RuleDetailBO ruleDetail = new RuleDetailBO();
            //    Helper.CopyPropertyValues(eo, ruleDetail);
            //    ruleDetailInfo.Add(ruleDetail);
            //}

            return ruleDetailInfo;
        }



        public List<RuleDetailBO> GetRuleDetailsListByTestCase(int testCaseId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetRuleDetailsListByTestCase(testCaseId);

            ruleDetailInfo = new List<RuleDetailBO>();
            RuleDetailBO ruleDetail = null;

            entityEnumerable.ToList().ForEach(eo =>
            {
                ruleDetail = new RuleDetailBO();
                Helper.CopyPropertyValues(eo, ruleDetail);
                ruleDetailInfo.Add(ruleDetail);
            });

            //foreach (EntityObject eo in entityEnumerable)
            //{
            //    RuleDetailBO ruleDetail = new RuleDetailBO();
            //    Helper.CopyPropertyValues(eo, ruleDetail);
            //    ruleDetailInfo.Add(ruleDetail);
            //}

            return ruleDetailInfo;
        }


        public bool UpdateData(List<RuleDetailBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(RuleDetailBO entityData)
        {
            throw new NotImplementedException();
        }
    }
}
