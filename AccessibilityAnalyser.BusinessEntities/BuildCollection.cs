﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class BuildCollection :IEntityCollection<BuildBO>
    {
        IDataAdapter entityStore = null;
        List<BuildBO> buildInfo = null;

        public BuildCollection()
        {
            //entityStore = new SQLDataEntitiesStore();

            entityStore = new DataStoreFactory().GetDataStore();
        }

        public BuildBO GetSingle(int ObjectId)
        {
            EntityObject buildObject = entityStore.GetBuild(ObjectId);

            BuildBO build = new BuildBO();
            Helper.CopyPropertyValues(buildObject, build);

            return build;
        }

        public List<BuildBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetBuilds();
            buildInfo = new List<BuildBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                BuildBO build = new BuildBO();
                Helper.CopyPropertyValues(eo, build);
                buildInfo.Add(build);
            }

            return buildInfo;
        }


        public bool UpdateData(List<BuildBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(BuildBO entityData)
        {
            return entityStore.CreateBuild(entityData);
        }
    }
}
