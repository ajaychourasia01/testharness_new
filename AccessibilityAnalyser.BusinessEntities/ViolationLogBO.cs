﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ViolationLogBO : IViolationLog
    {
        IDataAdapter entityStore = null;

        public ViolationLogBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public int ViolationLogId { get; set; }
        public int ClientId { get; set; }
        public int BuildId { get; set; }
        public int RunId { get; set; }
        public int TestCaseId { get; set; }
        public string SourceURL { get; set; }
        public char Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
