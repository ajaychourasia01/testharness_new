﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.DataAdapter;
using System.Data.Objects.DataClasses;
using AccessibilityAnalyser.Common;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RunDetailCollection : IEntityCollection<RunBO>
    {
        IDataAdapter entityStore = null;
        List<RunBO> runDetailInfo = null;

        public RunDetailCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }


        public RunBO GetSingle(int ObjectId)
        {
            EntityObject ruleDetailObject = entityStore.GetRuleDetail(ObjectId);

            RunBO runDetail = new RunBO();
            Helper.CopyPropertyValues(ruleDetailObject, runDetail);

            return runDetail;
        }

        public List<RunBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetRuns();

            runDetailInfo = new List<RunBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                RunBO runDetail = new RunBO();
                Helper.CopyPropertyValues(eo, runDetail);
                runDetailInfo.Add(runDetail);
            }

            return runDetailInfo;
        }

        public bool UpdateData(List<RunBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(RunBO entityData)
        {
            return entityStore.CreateRun(entityData);
        }
    }
}
