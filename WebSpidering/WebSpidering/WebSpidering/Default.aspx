﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="WebSpidering._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to ASP.NET!
    </h2>
    <p>
        <asp:Label ID="Label1" runat="server" Text="Enter Url: "></asp:Label>   
    <asp:TextBox ID="txtUrl" runat="server" Width="381px"></asp:TextBox>
    <br></br>

        <asp:Label ID="Label2" runat="server" Text="Enter Level"></asp:Label>
        <asp:TextBox ID="txtLevel" runat="server"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Text="Count of urls:"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtUrlCount" runat="server"></asp:TextBox>
    </p>
    
    <p>
        
        <asp:Button ID="btnUrls" runat="server" Text="Get Url List" Width="76px" 
            onclick="btnUrls_Click" />  
        <asp:GridView ID="gdvUrl" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" Height="120px">
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </p>
    <p>
        
        &nbsp;</p>
    </asp:Content>
