﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Web.Services.DataContracts;
using AccessibilityAnalyser.BusinessEntities;
using System.Configuration;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Common;

namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TestServices" in code, svc and config file together.
    public class TestServices : ITestServices
    {
        List<ViolationBO> errorReport = new List<ViolationBO>();
        string logFileType = ConfigurationManager.AppSettings["LogFileType"];

        //public int TestValidation(List<string> urlNames, int clientId, int buildId, int runId, int violationPerBatch)
        //{
        //    int status = 0;

        //    try
        //    {
        //        RuleValidator ruleValidator = new RuleValidator();
        //        status = ruleValidator.TestValidation(urlNames, clientId, buildId, runId,violationPerBatch);
        //    }
        //    catch (TimeoutException timeOutEx)
        //    {
        //        LogException.CatchException(timeOutEx, "TestService", "TestValidation");
        //    }
        //    catch (CommunicationException communicationEx)
        //    {
        //        LogException.CatchException(communicationEx, "TestService", "TestValidation");
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "TestService", "TestValidation");
        //    }

        //    return status;

        //}

        public List<TestCase> GetTestCases(string clientName, string siteName, string multipleChoice, int runId, int buildId)
        {
            List<TestCase> getTestCases = new List<TestCase>();

            try
            {
                getTestCases = Testing.GetTestCases(clientName, siteName, multipleChoice, runId, buildId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetTestCases");
            }
            return getTestCases;
        }

        public List<ClientBO> GetClients(string searchText)
        {
            List<ClientBO> getClients = new List<ClientBO>();

            try
            {
                getClients = Testing.GetClients(searchText);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetClients");
            }
            return getClients;
        }
        public List<ClientBO> GetClientSingle(string ClientName)
        {
            List<ClientBO> getClients = new List<ClientBO>();

            try
            {
                getClients = Testing.GetClientSingle(ClientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetClients");
            }
            return getClients;
        }

        public Violations GetViolations(int clientId, int buildId, int runId, int ruleDetailId, string violationType,int skip,int testCaseId)
        {
            //List<ViolationBO> getVioaltions = new List<ViolationBO>();
            var violations = new Violations();

            try
            {
                violations = Testing.GetViolations(clientId, buildId, runId, ruleDetailId, violationType, skip, testCaseId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetViolations");
            }
            return violations;
        }

        public bool UpdateViolation(string violationDescription, string violationRecommendation, int violationId, string status)
        {
            bool isUpdated = false;
            try
            {
                isUpdated = Testing.UpdateViolation(violationDescription, violationRecommendation, violationId, status);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "UpdateViolation");
            }
            return isUpdated;
        }

        public bool UpdateViolationForManual(string violationDescription, string violationRecommendation, string violationName, string sourceURL, string sourceElement, int violationId, string status)
        {
            bool isUpdated = false;
            try
            {
                isUpdated = Testing.UpdateViolationForManual(violationDescription, violationRecommendation, violationName, sourceURL, sourceElement, violationId, status);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "UpdateViolationForManual");
            }
            return isUpdated;
        }

        public int GetViolationCount(int ClientID)
        {
            int vioaltionCount = 0;
            try
            {
                vioaltionCount = Testing.GetViolationCount(ClientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetViolationCount");
            }
            return vioaltionCount;
        }

        public ViolationBO GetViolation(List<ViolationBO> violations, int violationId)
        {
            ViolationBO getVioaltion = new ViolationBO();
            try
            {
                getVioaltion = Testing.GetViolation(violations, violationId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetViolation");
            }

            return getVioaltion;
        }

        public List<SiteBO> GetSiteList(string searchText, string clientName)
        {
            List<SiteBO> getSiteList = new List<SiteBO>();

            try
            {
                getSiteList = Testing.GetSiteList(searchText, clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetSiteList");
            }
            return getSiteList;
        }

        public List<Build> GetBuilds(string clientName, string siteName)
        {
            List<Build> getBuilds = new List<Build>();
            try
            {
                getBuilds = Testing.GetBuilds(clientName, siteName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetBuilds");
            }

            return getBuilds;
        }

        public bool CreateViolation(string violationName, string violationDescription, int runId, string siteName, string violationSourceElement,
                                   int violationSourceLineNumber, string violationRecommendation, string status, int clientId, int buildId,string TestCaseName,int violationlogID)
        {
            bool isCreated = false;
            try
            {
                isCreated = Testing.CreateViolation(violationName, violationDescription, runId,
                                    siteName, violationSourceElement, violationSourceLineNumber,
                                    violationRecommendation, status, clientId,
                                    buildId, TestCaseName, violationlogID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "CreateViolation");
            }
            return isCreated;
        }

        public char GetViolationLogStatus()
        {
            char statusVal = 'C';
            try
            {
                statusVal = Testing.GetViolationLogStatus();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestService", "GetViolationLogStatus");
            }
            return statusVal;
        }
    }
}
