﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;

namespace AccessibilityAnalyser.Web.Services.DataContracts
{
    
    [DataContract]
    [KnownType(typeof(ExecutionToolbar))]
    public class ExecutionToolbar
    {
        [DataMember]
        public StringBuilder str { get; set; }

    }
}