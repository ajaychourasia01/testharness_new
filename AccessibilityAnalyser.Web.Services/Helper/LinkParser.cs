using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;

namespace AccessibilityAnalyser.Web.Services.Helper
{
    public class LinkParser
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LinkParser()
        {            
        }

        #endregion
        #region Constants
        private string _LINK_REGEX = "href=\"[a-zA-Z0-9=?./:&_;-]+\"";
        List<string> fileTypes = null;
        #endregion
        #region Private Instance Fields

        private List<UrlFormat> _goodUrls = new List<UrlFormat>();        
        private List<UrlFormat> _externalUrls = new List<UrlFormat>();        

        #endregion
        #region Public Properties

        public List<UrlFormat> InternalUrls
        {
            get { return _goodUrls; }
            set { _goodUrls = value; }
        }

        public List<UrlFormat> ExternalUrls
        {
            get { return _externalUrls; }
            set { _externalUrls = value; }
        }              

        #endregion

        /// <summary>
        /// Returns url extensions that must be skipped from crawling.
        /// </summary>
        /// <returns>List of extension types to skip</returns>
        public List<string> GetFileTypes()
        {
            fileTypes = new List<string>();
            fileTypes.Add(".jpg");
            fileTypes.Add(".gif");
            fileTypes.Add(".pdf");
            fileTypes.Add(".xml");
            fileTypes.Add(".css");
            fileTypes.Add("javascript");
            fileTypes.Add(".js");
            fileTypes.Add(".png");
            fileTypes.Add(".mspx");
            fileTypes.Add(".ico");
            fileTypes.Add("./");
            fileTypes.Add("mailto:");            

            return fileTypes;
        }

        /// <summary>
        /// A case insenstive replace function.
        /// </summary>
        /// <param name="originalString">The string to examine.(HayStack)</param>
        /// <param name="oldValue">The value to replace.(Needle)</param>
        /// <param name="newValue">The new value to be inserted</param>
        /// <returns>A string</returns>
        public static string CaseInsenstiveReplace(string originalString, string oldValue, string newValue)
        {
            Regex regEx = new Regex(oldValue,
               RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return regEx.Replace(originalString, newValue);
        }

        /// <summary>
        /// Parses a page looking for links.
        /// </summary>
        /// <param name="page">The page whose text is to be parsed.</param>
        /// <param name="sourceUrl">The source url of the page.</param>
        public void ParseLinks(string page, UrlFormat sourceUrl)
        {
            MatchCollection matches = Regex.Matches(page, _LINK_REGEX);
            fileTypes = GetFileTypes();

            for (int i = 0; i <= matches.Count - 1; i++)
            {
                Match anchorMatch = matches[i];

                if (anchorMatch.Value == String.Empty)                
                    continue;                

                string foundHref = null;
                
                try
                {

                    foundHref = anchorMatch.Value.Replace("href=\"", "");
                    foundHref = foundHref.Substring(0, foundHref.IndexOf("\""));

                    if (foundHref.StartsWith("/") || (foundHref.ToLower().StartsWith("http") == false && foundHref.ToLower().StartsWith("https") == false))
                    {
                        string[] urlSplit = sourceUrl.UrlName.Split(new char[] { '/' }, StringSplitOptions.None);
                        string rebuildUrl = string.Empty;

                        if (foundHref.StartsWith("../"))
                        {                            
                            for (int iSplitIndex = 0; iSplitIndex < urlSplit.Length - 2; iSplitIndex++)
                            {
                                rebuildUrl += urlSplit[iSplitIndex] + "/";
                            }

                            foundHref = rebuildUrl + foundHref.Replace("../", "");
                        }
                        else if (!foundHref.StartsWith("/"))
                        {
                            rebuildUrl = sourceUrl.UrlName.Substring(0, sourceUrl.UrlName.LastIndexOf('/') + 1);
                            foundHref = rebuildUrl + foundHref;                            
                        }
                        else
                        {                            
                            for (int iSplitIndex = 0; iSplitIndex < 3; iSplitIndex++)
                            {
                                rebuildUrl += urlSplit[iSplitIndex] + "/";
                            }

                            foundHref = rebuildUrl + foundHref;                            
                        }
                    }



                    if (foundHref.EndsWith("/"))
                        foundHref = foundHref.Substring(0, foundHref.Length - 1);

                    if (string.IsNullOrEmpty(foundHref))
                        continue;

                    string tempUrl = foundHref.Substring(foundHref.IndexOf("//") + 2, foundHref.Length - (foundHref.IndexOf("//") + 2)).Replace("//", "/");
                    foundHref = foundHref.Substring(0, foundHref.IndexOf("//") + 2) + tempUrl;

                    foundHref = foundHref.IndexOf("&amp;", StringComparison.OrdinalIgnoreCase) > -1 ? CaseInsenstiveReplace(foundHref,"&amp;","&") : foundHref;
                }
                catch (Exception exc)
                {
                    continue;
                }

                UrlFormat foundUrl = new UrlFormat(foundHref);

                if (IsValidUrl(foundHref))
                {
                    bool isExternal = IsExternalUrl(foundHref);

                    if (isExternal)
                    {
                        foundUrl.IsExternal = true;
                        ExternalUrls.Add(foundUrl);
                    }
                    else
                    {
                        foundUrl.IsExternal = false;
                        InternalUrls.Add(foundUrl);
                    }                    
                }
            }
        }

        /// <summary>
        /// Verifies the url
        /// </summary>
        /// <param name="url">Url value</param>
        /// <returns>
        /// true  : if a valid url 
        /// false : if a bad url
        /// </returns>
        public bool IsValidUrl(string url)
        {
            bool retVal = true;
            foreach (string fileType in fileTypes)
            {
                if (url.ToLower().IndexOf(fileType) >= 0)
                    retVal = false;
            }

            return retVal;
        }

        /// <summary>
        /// Is the url to an external site?
        /// </summary>
        /// <param name="url">The url whose externality of destination is in question.</param>
        /// <returns>Boolean indicating whether or not the url is to an external destination.</returns>
        private bool IsExternalUrl(string url)
        {
            if (url.IndexOf(UrlFormat.Authority) > -1)
            {
                return false;
            }

            return true;
        }
    }
}
