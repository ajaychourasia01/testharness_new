﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
//using RA.EnterpriseLibrary.Logging;

namespace AccessibilityServices
{
    public class Crawling
    {
        List<string> fileTypes = null;
        const int configuredCount = 50;
        public static int taskCount = 0;

        //static RA.EnterpriseLibrary.Logging.IRALogger Logger = RA.EnterpriseLibrary.Logging.InfrastructureParts.Instance.Logger;


        public List<urlFormat> GetUrls(urlFormat url, string allowExternal)
        {
            List<urlFormat> hrefTags = new List<urlFormat>();
            List<string> UrlList = new List<string>();
            try
            {
                if (!allowExternal.Equals("External"))
                {
                    hrefTags.Add(url);
                }

                UrlList.Add(url.urlName);
                if (!url.IsScrawled)
                {
                    string baseSiteName = GetBaseSiteName(url.urlName);

                    fileTypes = GetFileTypes();
                    url.urlName = ValidAbsoluteUrl(url.urlName);

                    int count = 0;
                    var doc = new HtmlWeb().Load(url.urlName);

                    if (!url.urlName.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
                    {

                        HtmlNodeCollection col = doc.DocumentNode.SelectNodes("//a[@href]");

                        if (col != null && col.Count > 0)
                        {
                            foreach (HtmlNode link in col)
                            {
                                HtmlAttribute att = link.Attributes["href"];

                                if (att.Value != "#")
                                {
                                    string AbsoluteUrl = GetAbsoluteUrl(att.Value, url.urlName).Trim('/').ToLower();
                                    if (hrefTags.IndexOf(new urlFormat() { urlName = AbsoluteUrl }) == -1 &&
                                        //&& AbsoluteUrl.IndexOf(baseSiteName.Substring(baseSiteName.IndexOf("://"))) >= 0                            
                                        //(allowExternal ? true : AbsoluteUrl.IndexOf("://" + baseSiteName) >= 0)
                                        (allowExternal.Equals("All") || (allowExternal.Equals("Internal")
                                        && AbsoluteUrl.IndexOf("://" + baseSiteName) >= 0) ||
                                        (allowExternal.Equals("External") && AbsoluteUrl.IndexOf("://" + baseSiteName) < 0))
                                        && ValidUrl(AbsoluteUrl) && !UrlList.Contains(AbsoluteUrl))
                                    {
                                        if (AbsoluteUrl == url.urlName)
                                        {
                                            urlFormat newUrl = new urlFormat() { urlName = AbsoluteUrl, IsScrawled = true };
                                            count = count + 1;
                                            hrefTags.Add(newUrl);
                                        }
                                        else
                                        {
                                            urlFormat newUrl = new urlFormat() { urlName = AbsoluteUrl, IsScrawled = false };
                                            hrefTags.Add(newUrl);
                                            count = count + 1;
                                        }
                                        UrlList.Add(AbsoluteUrl);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
               // Logger.Log("Error : " + exp.InnerException, "UIBlock");
                throw exp;
            }
            return hrefTags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName"></param>
        /// <returns></returns>
        private string GetBaseSiteName(string siteName)
        {
            string baseSiteAddress = string.Empty;
            try
            {
                string[] splitAddress = siteName.Split('/');

                baseSiteAddress = splitAddress[2];
                if (string.IsNullOrEmpty(baseSiteAddress))
                {
                    baseSiteAddress = splitAddress[4];
                }
            }
            catch { }
            return baseSiteAddress;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool ValidUrl(string url)
        {
            bool retVal = true;
            foreach (string fileType in fileTypes)
            {
                if (url.ToLower().IndexOf(fileType) >= 0)
                    retVal = false;
            }

            return retVal;
        }


        /// <summary>
        /// Checks if the input url is a Valid url
        /// </summary>
        /// <param name="urls"></param>
        /// <returns></returns>
        public static string ValidateUrl(string urls)
        {
            string regex = @"^((http)://|(https)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$";
            Match urlMatch = Regex.Match(urls, regex);
            return urlMatch.Success ? urls : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string ValidAbsoluteUrl(string url)
        {
            string newurl = url;
            if (url.StartsWith("http:////", StringComparison.OrdinalIgnoreCase))
            {
                newurl = url.Replace("http:////", "http://");
            }
            if (url.StartsWith("https:////", StringComparison.OrdinalIgnoreCase))
            {
                newurl = url.Replace("https:////", "https://");
            }
            return newurl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string> GetFileTypes()
        {
            fileTypes = new List<string>();
            fileTypes.Add(".jpg");
            fileTypes.Add(".gif");
            fileTypes.Add(".pdf");
            fileTypes.Add(".xml");
            fileTypes.Add(".css");
            fileTypes.Add("javascript");
            fileTypes.Add(".js");
            fileTypes.Add(".png");
            fileTypes.Add(".mspx");
            fileTypes.Add("./");
            fileTypes.Add("mailto:");
            return fileTypes;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="Parenturl"></param>
        /// <returns></returns>
        public static string GetAbsoluteUrl(string url, string Parenturl)
        {
            if (url.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
                || url.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return url;
            }

            string strDomain = Parenturl.Substring(Parenturl.IndexOf("://") + 3);
            if (strDomain.Contains('/'))
            {
                if (url.StartsWith("/"))
                {
                    strDomain = strDomain.Substring(0, strDomain.IndexOf('/'));
                }
                else
                {
                    strDomain = strDomain.Substring(0, strDomain.LastIndexOf('/'));
                }
            }

            return "http://" + strDomain + "/" + url.TrimStart('/');
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SiteUrl"></param>
        /// <param name="depthLevel"></param>
        /// <returns></returns>
        public List<string> GetUrlList(string SiteUrl, int depthLevel, string allowExternal)
        {
            List<string> lstFinalUrl = new List<string>();
            List<urlFormat> lstInterUrl = new List<urlFormat>();

            urlFormat urlList = new urlFormat { urlName = SiteUrl.ToString().Trim().ToLower() };

            lstInterUrl = new List<urlFormat>();
            lstInterUrl.Add(urlList);

            for (int i = 1; i <= depthLevel; i++)
            {
                lstInterUrl = ParallelProcess(lstInterUrl, allowExternal);

                foreach (urlFormat item in lstInterUrl)
                {
                    if (!(lstFinalUrl.Exists(x => x == item.urlName.Trim().ToLower())))
                    {
                        //Logger.Log(item.urlName + " " + lstFinalUrl.Count, "UIBlock");
                        lstFinalUrl.Add(item.urlName);
                    }
                }
            }

            //Checks if the urls are valid and then adds to the list
            lstFinalUrl = (from validUrls in lstFinalUrl
                           let Vurl = ValidateUrl(validUrls)
                           where (!String.IsNullOrEmpty(Vurl))
                           select Vurl).ToList();

            return lstFinalUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstInitialUrls"></param>
        /// <returns></returns>
        public List<urlFormat> ParallelProcess(List<urlFormat> lstInitialUrls, string allowExternal)
        {
            List<urlFormat> lstInterUrls = new List<urlFormat>();
            List<urlFormat> lstResultUrls = new List<urlFormat>();
            Crawling objScrawling = new Crawling();

            //making asyn call
            var asyncCrawling = new Action<urlFormat>((urlString) =>
            {
                lstInterUrls = GetUrls(urlString, allowExternal);
            });

            var threadCallBack = new Action(() =>
            {
                if ((!lstInterUrls.Equals(null)) && lstInterUrls.Count > 0)
                {
                    foreach (urlFormat urlItem in lstInterUrls)
                    {
                        lstResultUrls.Add(urlItem);
                    }
                }

                taskCount--;

            }
            );


            int cnt = 0;
            Task[] tasks = new Task[lstInitialUrls.Count];

            //Begining and invoking async call.
            foreach (urlFormat item in lstInitialUrls)
            {

                // if (taskCount < configuredCount)
                //{
                taskCount++;
               // Logger.Log("Thread Count : " + taskCount.ToString(), "UIBlock");

                tasks[cnt] = Task.Factory.FromAsync(asyncCrawling.BeginInvoke, asyncCrawling.EndInvoke, item, asyncCrawling)
                   .ContinueWith(_ => threadCallBack());

                //tasks[cnt].Wait();

                cnt++;

            }


            Task.WaitAll(tasks);

            return lstResultUrls;
        }



    }

    /// <summary>
    /// 
    /// </summary>
    public class urlFormat
    {
        public string urlName { get; set; }
        public bool IsScrawled { get; set; }
    }
}

