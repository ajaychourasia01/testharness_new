﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using HtmlAgilityPack;
using System.Text;


namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExecuteTestValidation" in both code and config file together.
    [ServiceContract]    
    public interface IExecuteTestValidation
    {
        [OperationContract]
        int ExecuteTestValidationParam(string selectedURLArray, int clientId, int buildId, int runId, int violationPerBatch, StringBuilder html);

        [OperationContract]
        int GetHtmlExtracted(string selectedURLArray, int clientId, int buildId, int runId);
    }
}
