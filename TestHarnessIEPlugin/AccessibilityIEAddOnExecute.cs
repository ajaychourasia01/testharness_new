﻿using Microsoft.Win32;
using mshtml;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace AccessibilityAnalyser.BrowserPlugIn
{
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("23c3821d-9196-4e16-813d-d0e2ebef84e2")]
    [ProgId("Brillio.AccessibilityTesting.Execution")]
    public class AccessibilityIEAddOnExecute: IObjectWithSite, IOleCommandTarget
    {
        IWebBrowser2 browser;
        private object site;
        private const string EXECUTIONSERVICEURL = "http://localhost:55437/Services/ExecuteTestValidation.svc";
        private const string EXECUTIONSERVICEBINDINGNAME = "BasicHttpBinding_IExecuteTestValidation";
        private const int VIOLATIONSPERBATCH = 100000;

        [Guid("6D5140C1-7436-11CE-8034-00AA006009FA")]
        [InterfaceType(1)]
        public interface IServiceProvider
        {
            int QueryService(ref Guid guidService, ref Guid riid, out IntPtr ppvObject);
        }

        #region Implementation of IObjectWithSite
        int IObjectWithSite.SetSite(object site)
        {
            System.Diagnostics.Debugger.Launch();
            this.site = site;

            if (site != null)
            {
                var serviceProv = (IServiceProvider)this.site;
                var guidIWebBrowserApp = Marshal.GenerateGuidForType(typeof(IWebBrowserApp)); // new Guid("0002DF05-0000-0000-C000-000000000046");
                var guidIWebBrowser2 = Marshal.GenerateGuidForType(typeof(IWebBrowser2)); // new Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E");
                IntPtr intPtr;
                serviceProv.QueryService(ref guidIWebBrowserApp, ref guidIWebBrowser2, out intPtr);

                browser = (IWebBrowser2)Marshal.GetObjectForIUnknown(intPtr);
            }
            else
            {
                browser = null;
            }
            return 0;
        }

        int IObjectWithSite.GetSite(ref Guid guid, out IntPtr ppvSite)
        {
            IntPtr punk = Marshal.GetIUnknownForObject(browser);
            int hr = Marshal.QueryInterface(punk, ref guid, out ppvSite);
            Marshal.Release(punk);
            return hr;
        }

        #endregion

        int IOleCommandTarget.QueryStatus(IntPtr pguidCmdGroup, uint cCmds, ref OLECMD prgCmds, IntPtr pCmdText)
        {
            return 0;
        }

        int IOleCommandTarget.Exec(IntPtr pguidCmdGroup, uint nCmdID, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
        {
            System.Diagnostics.Debugger.Launch();

            try
            {
                // Accessing the document from the command-bar.
                var document = browser.Document as IHTMLDocument2;
                var window = document.parentWindow;
                //var result = window.execScript(@"alert('Inside execution');");
                string htmlContent = (browser.Document as IHTMLDocument3).documentElement.innerHTML; 
                string browserUrl = document.url;

                ValidateViolations(browserUrl, htmlContent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return 0;
        }

        private void ValidateViolations(string browserUrl, string htmlContent)
        {
            System.Diagnostics.Debugger.Launch();
            ExecutionTestReference.ExecuteTestValidationClient execTestClient = Helper.CreateClient<ExecutionTestReference.ExecuteTestValidationClient>
                        (EXECUTIONSERVICEURL, EXECUTIONSERVICEBINDINGNAME);
            try
            {
                var config = Helper.LoadConfigurationDetails();
                string urls = string.Concat(browserUrl, "~");
                StringBuilder sbHtml = new StringBuilder(htmlContent);
                var status = execTestClient.ExecuteTestValidationParam(urls, config.ClientId, config.BuildId, config.RunId, VIOLATIONSPERBATCH, sbHtml);
                if (status == 30)
                {
                    MessageBox.Show("Execution completed. Execution results can be viewed in the web application.", "Accessibility Test Execution", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (execTestClient != null)
                {
                    execTestClient.Close();
                }
            }
        }


        #region Registering with regasm
        public static string RegBHO = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";
        public static string RegCmd = "Software\\Microsoft\\Internet Explorer\\Extensions";

        [ComRegisterFunction]
        public static void RegisterBHO(Type type)
        {
            string guid = type.GUID.ToString("B");
            string iconFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Brillio", "TestHarnessPlugin");
            
            // BHO
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegBHO, true);
                if (registryKey == null)
                    registryKey = Registry.LocalMachine.CreateSubKey(RegBHO);
                RegistryKey key = registryKey.OpenSubKey(guid);
                if (key == null)
                    key = registryKey.CreateSubKey(guid);
                key.SetValue("Alright", 1);
                registryKey.Close();
                key.Close();
            }

            // Command
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegCmd, true);
                if (registryKey == null)
                    registryKey = Registry.LocalMachine.CreateSubKey(RegCmd);
                RegistryKey key = registryKey.OpenSubKey(guid);
                if (key == null)
                    key = registryKey.CreateSubKey(guid);
                key.SetValue("ButtonText", "Accessibility Testing Execution");
                key.SetValue("CLSID", "{2C9B1B7F-6871-484A-A4AD-7385A2B38FD4}");
                key.SetValue("ClsidExtension", guid);
                //key.SetValue("Icon", @"E:\Projects\TestHarness\TestHarness_Final\TestHarness\AllBinaries\Release\Execution.ico");
                key.SetValue("Icon", Path.Combine(iconFilePath, "Execution.ico"));
                key.SetValue("HotIcon", Path.Combine(iconFilePath, "Execution.ico"));
                key.SetValue("Default Visible", "Yes");
                key.SetValue("MenuText", "&Accessibility Testing Execution");
                key.SetValue("ToolTip", "Accessibility Testing Execution");
                //key.SetValue("KeyPath", "no");
                registryKey.Close();
                key.Close();
            }
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type type)
        {
            string guid = type.GUID.ToString("B");
            // BHO
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegBHO, true);
                if (registryKey != null)
                    registryKey.DeleteSubKey(guid, false);
            }
            // Command
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegCmd, true);
                if (registryKey != null)
                    registryKey.DeleteSubKey(guid, false);
            }
        }
        #endregion
    }
}
