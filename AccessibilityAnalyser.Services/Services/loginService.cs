﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Common;

namespace Accessibility.ServicesLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in code, svc and config file together.
    public class LoginService : ILoginService
    {
        public List<LoginBO> GetUsers()
        {
            List<LoginBO> getUsers = new List<LoginBO>();

            try
            {
                getUsers = Login.GetUsers();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "LoginService", "GetUsers");
            }

            return getUsers;
        }
    }
}
