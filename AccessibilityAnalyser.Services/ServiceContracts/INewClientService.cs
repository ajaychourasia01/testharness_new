﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewClientService" in both code and config file together.
    /// <summary>
    /// Interfaces for New Client Service
    /// </summary>
    public interface INewClientService
    {
        
        List<ClientBO> GetClientList(string searchText);
        
        
        List<SiteBO> GetSiteList(string searchText, int clientID);

        
        List<BuildBO> GetBuildList(string searchText, int siteID);

        
        List<RunBO> GetRunList(string searchText, int buildID);

        
        int GetClientID(string clientName);

        
        int GetSiteID(string url, int clientID);

        
        int GetBuildID(string buildName, int siteID);

        
        int GetRunID(string runName, int buildID, int clientID);

        
        bool CreateClient(string clientName);

        
        bool CreateSite(string siteName, int clientId);

        
        bool CreateBuild(string buildName, int siteId);

        
        bool CreateRun(int clientId, int buildId, string runName);

        
        bool CreateTestCase(string testCaseName, int buildId, string strTestcaseDesc);

        
        bool CreateTestCaseMap(int ruleDetailId, int testCaseId, int iPriority);

        
        List<TestCaseBO> GetAllDistinctTestCases();

        
        List<RuleDetailBO> GetAllDistinctRuleDetails();

        
        List<TestCaseBO> GetAllTestCases(int buildId);

        
        bool UpdateTestCase(TestCaseBO TestCaseData);

        
        bool DeleteTestCaseRuleMap(TestCaseRuleMapBO TestCaseRuleMapData);

        
        bool DeleteMultipleTestCaseRuleMap(List<TestCaseRuleMapBO> TestCaseRuleMapData);

        
        List<TestCaseRuleMapBO> GetAllTestCaseRuleMap(int iTestCaseID);

        
        bool DeleteSingleTestCase(int iTestCaseId);

        
        IEnumerable<StandardBO> GetAllGroups();

        
        List<RuleDetails> GetRuleDetails(string groupID);

        
        List<RuleDetails> GetAllRuleDetails();

        
        TestCaseBO GetSingleTestCase(int iTestcaseId);
    }
}
