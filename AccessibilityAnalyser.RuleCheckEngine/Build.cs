﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.RuleEngine
{
    public class Build
    {
        public int BuildId
        { 
            get; 
            set; 
        }

        public string BuildName 
        { 
            get; 
            set; 
        }

        public string BuildDescription 
        { 
            get; 
            set; 
        }

        public int SiteId
        { 
            get; 
            set; 
        }

        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public List<RunBO> runDetails;

        public Build()
        {
            runDetails = new List<RunBO>();
        }

        public void AddRunDetail(RunBO runDetailTemp)
        {
            RunBO runDetail = new RunBO
            {
                ClientId = runDetailTemp.ClientId,
                BuildId = runDetailTemp.BuildId,
                RunId = runDetailTemp.RunId,
                RunName = runDetailTemp.RunName,               
                IsActive = runDetailTemp.IsActive,
                CreatedDate = runDetailTemp.CreatedDate,
                CreatedBy = runDetailTemp.CreatedBy,
                UpdateDate = runDetailTemp.UpdateDate,
                UpdateBy = runDetailTemp.UpdateBy
            };

            runDetails.Add(runDetail);
        }
    }
}

	
