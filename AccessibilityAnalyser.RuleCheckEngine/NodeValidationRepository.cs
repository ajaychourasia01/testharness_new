﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using AccessibilityAnalyser.Common;
using System.Resources;

namespace AccessibilityAnalyser.RuleEngine
{
    public class NodeValidationRepository
    {
        static ResourceManager resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);

        /// <summary>
        /// check if the element has attribute attr defined
        /// return true if has, otherwise, return false
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        public static bool HasAttribute(HtmlNode node, string attr)
        {
            return (node.Attributes[attr.ToLower()] == null) ? false : true;
        }

        /// <summary>
        /// Check for alt attribute
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static bool HasAltAttribute(HtmlNode node)
        {
            return (node.Attributes["alt"] == null) ? false : true;
        }
        /// <summary>
        /// return the length of the trimed value of specified attribute
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private static int GetAttributeTrimedValueLength(HtmlNode node, string attr)
        {
            return node.GetAttributeValue(attr, null) != null ? node.GetAttributeValue(attr, null).Trim().Length : -1;
        }

        /// <summary>
        /// return the value of the specified attribute
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        public static string GetAttributeValue(HtmlNode node, string attr)
        {
            return node.GetAttributeValue(attr, null);
        }

        /// <summary>
        /// return language code that is defined in the given html
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static string GetLangCode(HtmlDocument doc)
        {
            HtmlNode node = doc.DocumentNode.SelectSingleNode(resourceManager.GetString("HtmlTag"));

            return (node.Attributes["lang"] != null) ? node.Attributes["lang"].Value : string.Empty;
        }

        /// <summary>
        /// return the height and width of the image
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr">src</param>
        /// <returns></returns>
        private static Dictionary<string, int> GetImageWidthAndHeight(HtmlNode node)
        {
            Dictionary<string, int> imageHeightAndWidth = new Dictionary<string, int>();

            if (node.Attributes["Height"] != null)
            {
                imageHeightAndWidth.Add("Height", Convert.ToInt32(node.Attributes["height"].Value.Replace("px", "")));
            }

            if (node.Attributes["Width"] != null)
            {
                imageHeightAndWidth.Add("Width", Convert.ToInt32(node.Attributes["width"].Value.Replace("px", "")));
            }

            return imageHeightAndWidth;
        }

        /// <summary>
        /// return the value of the specified attribute in lower case
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        public static string GetAttributeValueInLowerCase(HtmlNode node, string attr)
        {
            string attrValue = node.GetAttributeValue(attr, null) != null ? node.GetAttributeValue(attr, null).ToLower() : string.Empty;

            return attrValue;
        }

        /// <summary>
        /// return the length of the value of specified attribute
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private static int GetAttributeValueLength(HtmlNode node, string attr)
        {
            int attrLength = node.GetAttributeValue(attr, null) != null ? node.GetAttributeValue(attr, null).Length : 0;

            return attrLength;
        }

        /// <summary>
        /// return last 4 characters. Usually used to get file extension 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private static string GetLast4CharsFromAttributeValue(HtmlNode node, string attr)
        {
            string attValue = node.GetAttributeValue(attr, null);

            return attValue != null ? attValue.Substring(attValue.Length - 4) : string.Empty;
        }

        /// <summary>
        /// Returns the portion of string  specified by the start  and length  parameters.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static string GetSubstring(string value, int start, int length)
        {
            return value.Substring(start, length);
        }

        /// <summary>
        /// return the value of specified attribute as a number. 
        /// this is applicable for all elements with attribute (e.g: height / width) value as number.
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private static int GetAttributeValueAsNumber(HtmlNode node, string attr)
        {
            return node.GetAttributeValue(attr, null) != null ? Convert.ToInt32(node.GetAttributeValue(attr, null)) : -1;
        }

        /// <summary>
        /// return the trimed value of inner text
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static string GetInnerText(HtmlNode node)
        {
            return node.InnerText.Trim();
        }

        /// <summary>
        /// return the length of the trimed inner text of specified attribute
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static int GetInnerTextLength(HtmlNode node)
        {
            return node.InnerText.Trim().Length;
        }

        /// <summary>
        ///  check if associated label of element has text
        ///  return true if has, otherwise, return false
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static bool AssociatedLabelHasText(HtmlNode node, HtmlDocument doc)
        {
            string idAttrvalue = string.Empty;
            string forAttrvalue = string.Empty;

            // 1. The element has a "title" attribute
            if (node.Attributes["title"] != null && node.Attributes["title"].Value != string.Empty)
            {
                return true;
            }

            // 2. The element is contained by a "label" element
            HtmlNode parentNode = node.ParentNode;

            if (parentNode != null && parentNode.OriginalName == "label")
            {
                return true;
            }

            // 3. The element has an "id" attribute value that matches the "for" attribute value of a "label" 
            if (node.Attributes["id"] != null)
            {
                idAttrvalue = node.Attributes["id"].Value;

                foreach (HtmlNode forNode in doc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")))
                {
                    if (forNode.Attributes["for"] != null)
                    {
                        forAttrvalue = forNode.Attributes["for"].Value;
                    }

                    if (forAttrvalue == idAttrvalue)
                    {
                        return true;
                    }

                    // label contains an image with alt text
                    HtmlNode item = forNode.SelectSingleNode(resourceManager.GetString("ImageTag"));

                    if (item != null && item.Attributes["alt"] != null && !string.IsNullOrEmpty(item.Attributes["alt"].Value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// return the tag of the parent html tag
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static HtmlNode GetParentHTMLTag(HtmlNode node)
        {
            return node.ParentNode;
        }

        /// <summary>
        /// Check recursively if there are duplicate attr defined in children of node. 
        /// return true if there is, otherwise, false
        /// E.g: Id of an element should be unique when compared to childIds
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        private static bool HasDuplicateAttribute(HtmlNode node, string attr)
        {
            string id = string.Empty;
            string childNodeID = string.Empty;
            bool hasDuplicateAttribute = false;

            if (node.Attributes[attr] != null)
            {
                id = node.Attributes[attr].Value;
            }

            foreach (HtmlNode descendant in node.Descendants())
            {
                if (descendant.Attributes[attr] != null)
                {
                    childNodeID = descendant.Attributes[attr].Value;
                }

                if (id == childNodeID)
                {
                    hasDuplicateAttribute = true;
                    break;
                }
            }

            return hasDuplicateAttribute;
        }

        /// <summary>
        /// scan thru all the children and return the number of times that the specified html tag appears in all children 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        private static int GetNumOfTagInChildren(HtmlNode node, string tag)
        {
            return node.ChildNodes.Where(r => r.OriginalName.ToLower().Equals(tag.ToLower())).Count();
        }


        /// <summary>
        /// scan thru all the children, check if the given html tag exists and its inner text has content 
        /// return number of childrens.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        private static int GetNumOfTagInChildrenWithInnerText(HtmlNode node, string tag)
        {
            return node.ChildNodes.Where(r => r.OriginalName.ToLower().Equals(tag.ToLower())
                                         && r.InnerText.Trim().Length > 0).Count();
        }

        /// <summary>
        /// return html tag of the first child
        /// </summary>
        /// <param name="node">HtmlNode</param>
        /// <returns>string</returns>
        private static string GetFirstChildTag(HtmlNode node)
        {
            return node.FirstChild.OriginalName;
        }

        /// <summary>
        /// scan thru all the children and return the length of attribute value that the specified html tag appears children 
        /// </summary>
        /// <param name="node">HtmlNode</param>
        /// <param name="attr">string</param>
        /// <param name="tag">string</param>
        /// <returns>static int</returns>
        private static int getLengthOfAttributeValueWithGivenTagInChildren(HtmlNode node, string attr, string tag)
        {
            int attrLenth = 0;

            var attrValue = from i in node.ChildNodes.Where(r => r.OriginalName.ToLower().Equals(tag.ToLower()))
                            select new
                            {
                                Value = i.GetAttributeValue(attr, null)
                            };


            foreach (var item in attrValue)
            {
                if (item.Value != null)
                {
                    attrLenth = item.Value.Length;
                }
            }

            return attrLenth;
        }

        /// <summary>
        /// scan thru all the children and return the attribute value that the specified html tag appears in the children
        /// </summary>
        /// <param name="node">HtmlNode</param>
        /// <param name="attr">string</param>
        /// <param name="tag">string</param>
        /// <returns>string</returns>
        private static string getLowerCaseAttributeValueWithGivenTagInChildren(HtmlNode node, string attr, string tag)
        {
            string attributeValue = string.Empty;

            var attrValue = from i in node.ChildNodes.Where(r => r.OriginalName.ToLower().Equals(tag.ToLower()))
                            select new
                            {
                                Value = i.GetAttributeValue(attr, null)
                            };


            foreach (var item in attrValue)
            {
                if (item.Value != null)
                {
                    attributeValue = item.Value.ToLower();
                }
            }

            return attributeValue;
        }


        /// <summary>
        /// return the html tag of the next sibling
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static string GetNextSiblingTag(HtmlNode node)
        {
            return node.NextSibling.OriginalName;
        }

        /// <summary>
        /// check if there's given tag in children.
        /// return true if has, otherwise, false
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        private static bool HasTagInChildren(HtmlNode node, string tag)
        {
            foreach (HtmlNode childNode in node.ChildNodes)
            {
                if (childNode.OriginalName.Equals(tag.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///  Check recursively to find if element has a parent with tag
        ///  return true if found, otherwise, false
        /// </summary>
        /// <param name="node"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        private static bool HasParent(HtmlNode node, string tag)
        {
            foreach (HtmlNode ancestorItem in node.Ancestors())
            {
                foreach (HtmlNode childItem in ancestorItem.ChildNodes)
                {
                    if (childItem.OriginalName.ToLower().Equals(tag.ToLower()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        ///  return the html tag of the next sibling
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static string GetNextSiblingAttributeValueInLowerCase(HtmlNode node, string attr)
        {
            HtmlNode nextSiblingNode = node.NextSibling;

            return nextSiblingNode.Attributes[attr] != null ? nextSiblingNode.Attributes[attr].Value.ToLower() : string.Empty;
        }

        /// <summary>
        /// return the inner text of the next sibling. for example: if next sibling is "<a href="rex.html">[d]</a></p>",
        /// this function returns "[d]"
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attr"></param>
        /// <returns></returns>
        private static string GetNextSiblingInnerText(HtmlNode node, string attr)
        {
            HtmlNode nextSiblingNode = node.NextSibling;

            return nextSiblingNode.InnerText;
        }


        /// <summary>
        /// To verify that all the decorative images on the webpage have alt = "" attribute
        /// </summary>
        /// <param name="doc">HtmlDocument</param>
        /// <returns>Dictionary<string, string></returns>
        private static Dictionary<string, string> GetAllImagesWithBlankAltAttribute(HtmlDocument doc)
        {
            HtmlNodeCollection imgNode = doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag"));
            Dictionary<string, string> imgTagForBlankAltAttr = new Dictionary<string, string>();
            int counter = 0;

            if (imgNode != null)
            {
                var attrValue = from i in imgNode
                                select new
                                {
                                    Value = i.GetAttributeValue("alt", null),
                                    Id = i.Id,
                                    Line = i.Line
                                };

                foreach (var item in attrValue)
                {
                    if (item.Value != null)
                    {
                        if (item.Value.Trim().Equals(string.Empty))
                        {
                            counter++;

                            imgTagForBlankAltAttr.Add(string.Format("Id{0}", counter), item.Id);
                            imgTagForBlankAltAttr.Add(string.Format("LineNo{0}", counter), item.Line.ToString());
                        }
                    }
                }
            }

            return imgTagForBlankAltAttr;
        }
    }
}
