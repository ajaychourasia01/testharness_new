﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Reflection;
using System.Resources;
using HtmlAgilityPack;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.Interfaces;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Data;

namespace AccessibilityAnalyser.RuleEngine
{
    public class RuleValidator
    {
        #region Private Variables

        private IEnumerable<TestCaseRuleMapBO> testCaseRuleDetailMapping = null;
        public List<ViolationBO> violationReport = new List<ViolationBO>();
        ResourceManager resourceManager = null;
        List<ViolationLogData> objGlobalStatusList = new List<ViolationLogData>();

        #endregion Private Variables
        public RuleValidator()
        {
            resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);
        }
        #region Public Methods

        /// <summary>
        /// Method to get the list of rules based on testcase and  invoke the rule from DB
        /// </summary>
        /// <param name="urlNameToValidate"></param>
        /// <param name="ruleDetailListToValidate"></param>
        /// <param name="testCaseRuleMapping"></param>
        /// <param name="clientId"></param>
        /// <param name="buildId"></param>
        /// <param name="runId"></param>
        /// <returns>Bool</returns>
        private IList<IViolation> ExecuteValidation(List<string> urlNameToValidate, IEnumerable<RuleDetailBO> ruleDetailListToValidate,
     IEnumerable<TestCaseRuleMapBO> testCaseRuleMapping, int clientId, int buildId, int runId, int TestCaseID, string html)
        {
            HtmlDocument htmlContent = null;
            Dictionary<string, string> classProperties = new Dictionary<string, string>();
            IEnumerable<RuleDetailBO> automatedRuleDetails = null;
            List<ViolationBO> violationLog = new List<ViolationBO>();
            List<IViolation> allViolationLog = new List<IViolation>();
            IList<IViolation> tempViolationLog = null;
            Type assemblyForHTML;
            MethodInfo method;
            int violationLogId = 0;
            bool isPassed = true;

            try
            {

                testCaseRuleDetailMapping = testCaseRuleMapping;

                //Fecthing only automated rules for validation  *Duplicate Rules Not Invoked*
                automatedRuleDetails = from rd in ruleDetailListToValidate
                                       where rd.IsAutomated == true
                                       && rd.IsActive == true
                                       && rd.IsDuplicate == false
                                       select rd;




                try
                {
                    foreach (string urlName in urlNameToValidate)
                    {
                        Task[] tasks = new Task[automatedRuleDetails.Count()];

                        if (!String.IsNullOrEmpty(RuleEngineHelper.ValidateUrl(urlName)))
                        {
                            if (html.Equals(string.Empty))
                            {
                                htmlContent = HTMLExtractor.GetHtmlContent(urlName);
                            }
                            else
                            {
                                htmlContent = new HtmlDocument();
                                htmlContent.LoadHtml(html);
                            }

                            if (htmlContent == null)
                                continue;
                            try
                            {
                                if (htmlContent != null && !htmlContent.DocumentNode.InnerHtml.Contains("threw"))
                                {
                                    classProperties = CssParsing.GetStyleSheetClasses(htmlContent, RuleEngineHelper.GetBaseSiteName(urlName));

                                    violationLogId = GetViolationLogId(urlName, TestCaseID, clientId, buildId, runId);

                                    //Async function to be invoked for validation
                                    var asyncContentValidator = new Action<RuleDetailBO>((rdetail) =>
                                    {
                                        //tempViolationLog = new List<IViolation>();
                                        ViolationLogData objViolationLogData = new ViolationLogData();

                                        //Load the assembly to execute the validation Method
                                        assemblyForHTML = typeof(AccessibilityAnalyser.RuleEngine.ContentValidationRepository);

                                        //Get  MethodInfo by passing the assembly having validation methods whole HTMLContent. 
                                        method = getMethodToInvoke(assemblyForHTML, rdetail.MethodToInvoke);

                                        /*Added for fetching the Rule Name*/
                                        //int RuleID = from rd in ruleDetailListToValidate
                                        //                     where rd.IsAutomated == true
                                        //                     && rd.IsActive == true
                                        //                     && rd.IsDuplicate == false
                                        //                     select rd.RuleId;

                                        //var violationLstDup1 = (from b in buildList
                                        //                        join s in siteList on b.SiteId equals s.SiteId
                                        //                        join t in testCaseList on b.BuildId equals t.BuildId
                                        //                        join tr in testCaseRuleMapList on t.TestCaseId equals tr.TestCaseId
                                        //                        join r in ruleDetailsList on tr.RuleDetailId equals r.RuleDetailId
                                        //                        join r1 in ruleDetailsList on r.RuleDetailId equals r1.DuplicateRuleDetailId
                                        //                        join v in violationLst on tr.TRMappingId equals v.TRMappingId
                                        //                        where listTestCaseIds.Contains(t.TestCaseId) && t.BuildId == buildId && t.IsActive == true
                                        //                        select v).ToList();

                                        //fetches violations for a url against list of rules associated with the testcase
                                        //The below has been modified for 
                                        if (!rdetail.IsBaseUrl)
                                        {
                                            if (rdetail.RuleId == 13)
                                            {
                                                rdetail.IsBaseUrl = true;
                                                string baseurlName = RuleEngineHelper.GetBaseSiteName(urlName);
                                                object[] baseUrlRuleParameters = { htmlContent, classProperties, urlName, isPassed };
                                                violationLog = (List<ViolationBO>)method.Invoke(null, baseUrlRuleParameters);
                                                objViolationLogData.status = Convert.ToBoolean(baseUrlRuleParameters[3]);
                                            }
                                            else
                                            {
                                                object[] cssRuleParameters = { htmlContent, classProperties, isPassed };
                                                violationLog = (List<ViolationBO>)method.Invoke(null, cssRuleParameters);
                                                objViolationLogData.status = Convert.ToBoolean(cssRuleParameters[2]);
                                            }

                                            //object[] cssRuleParameters = { htmlContent, classProperties, isPassed};

                                            objViolationLogData.violationLogId = violationLogId;
                                            objViolationLogData.invokedMethodName = rdetail.MethodToInvoke;
                                            //objViolationLogData.status = Convert.ToBoolean(cssRuleParameters[2]);
                                            objGlobalStatusList.Add(objViolationLogData);

                                            tempViolationLog = createViolations(violationLog, rdetail, urlName, clientId, buildId, runId, violationLogId, ruleDetailListToValidate);
                                        }
                                        //for duplicate link labels rule
                                        else if (rdetail.IsBaseUrl)
                                        {
                                            object[] baseUrlRuleParameters = { htmlContent, classProperties, urlName, isPassed };
                                            violationLog = (List<ViolationBO>)method.Invoke(null, baseUrlRuleParameters);
                                            objViolationLogData.violationLogId = violationLogId;
                                            objViolationLogData.invokedMethodName = rdetail.MethodToInvoke;
                                            objViolationLogData.status = Convert.ToBoolean(baseUrlRuleParameters[3]);
                                            objGlobalStatusList.Add(objViolationLogData);

                                            tempViolationLog = createViolations(violationLog, rdetail, urlName, clientId, buildId, runId, violationLogId, ruleDetailListToValidate);
                                        }
                                    });

                                    var taskCallBack = new Action(() =>
                                    {
                                        if (tempViolationLog != null && tempViolationLog.Any())
                                        {
                                            allViolationLog.AddRange(tempViolationLog);

                                            //foreach (IViolation violation in tempViolationLog)
                                            //{
                                            //    allViolationLog.Add(violation);
                                            //}
                                        }
                                    });

                                    automatedRuleDetails.ToList().ForEach(ruleDetail => Task.Factory.FromAsync(asyncContentValidator.BeginInvoke, asyncContentValidator.EndInvoke, ruleDetail, asyncContentValidator).ContinueWith(_ => taskCallBack()).Wait());

                                    //foreach (RuleDetailBO ruleDetail in automatedRuleDetails)
                                    //{                                        
                                    //   Task.Factory.FromAsync(asyncContentValidator.BeginInvoke, asyncContentValidator.EndInvoke, ruleDetail, asyncContentValidator).ContinueWith(_ => taskCallBack()).Wait();
                                    //}                                    
                                }
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                    }
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                updateDecliendViolations(clientId, buildId, runId);
                LogException.CatchException(ex, "RuleValidator", "ExecuteValidation");
                throw ex;
            }
            finally
            {
                htmlContent = null;
                classProperties = new Dictionary<string, string>();
                automatedRuleDetails = null;
                violationLog = new List<ViolationBO>();
                assemblyForHTML = null;
                method = null;
            }

            return allViolationLog;
        }

        /// <summary>
        /// Method to get the testcase and invoke the Rules
        /// </summary>
        /// <param name="urlNames"></param>
        /// <param name="clientId"></param>
        /// <param name="buildId"></param>
        /// <param name="runId"></param>
        /// <returns>int</returns>
        public int TestValidation(List<string> urlNames, int clientId, int buildId, int runId, int violationPerBatch,string html="")
        {
            int status = 0;
            TestCaseCollection tc = new TestCaseCollection();
            PrepareRules ruleDetailsForValidation = new PrepareRules();
            IEnumerable<RuleDetailBO> ruleDetailsList = null;
            IEnumerable<TestCaseBO> testcaseList = null;
            ViolationCollection collection = new ViolationCollection();
            List<IViolation> violationLst = new List<IViolation>();
            IList<IViolation> tempViolationLst = new List<IViolation>();


            try
            {
                // Get all testcases based on build
                testcaseList = tc.GetRecords().Where(o => o.BuildId.Equals(buildId));
                Task[] mainTask = new Task[testcaseList.Count()];

                try
                {
                    //Runs execution for list of url per testcase
                    foreach (TestCaseBO testCaseBo in testcaseList)
                    {
                        //Gets rule details for the selected build and testcase for the client
                        ruleDetailsList = ruleDetailsForValidation.PrepareRulesToTest(buildId, testCaseBo.TestCaseId);

                        //Code to Bulk insert into Violation_Log Table
                        CreateViolationsLog(urlNames, testCaseBo.TestCaseId, clientId, buildId, runId);

                        tempViolationLst = ExecuteValidation(urlNames, ruleDetailsList, ruleDetailsForValidation.TestCaseRuleMapList, clientId, buildId, runId, testCaseBo.TestCaseId, html);
                        if (tempViolationLst != null && tempViolationLst.Any())
                        {
                            violationLst.AddRange(tempViolationLst);
                            //foreach (IViolation violation in tempViolationLst)
                            //{
                            //    violationLst.Add(violation);
                            //}
                        }
                    }

                }
                finally
                {
                    //foreach (var task in mainTask)
                    //{
                    //    task.Dispose();
                    //}
                }

                //Method to get distinct violationLogId values
                violationLst.Select(o => o.violationLogId).Distinct().ToList().ForEach(d =>
                {
                    UpdateViolationsLogStatus(d);
                });

                //violationLst contains Allready invoked rules 
                //Insert the set of records i.e violationLst to DB
                if (violationLst != null && violationLst.Any())
                {
                    ViolationCollection collectionObj1 = null;
                    int noOfBatch = violationLst.Count() > violationPerBatch ? violationLst.Count() / violationPerBatch : 0;

                    for (int i = 0; i <= noOfBatch; i++)
                    {
                        int skip = noOfBatch > 0 && i > 0 ? i * violationPerBatch : 0;
                        collectionObj1 = new ViolationCollection();
                        IList<IViolation> violationList1 = noOfBatch > 0 ? GetViolationsList(violationLst, skip, violationPerBatch) : violationLst;

                        collectionObj1.CreateViolations(violationList1);
                        status = violationList1.Count;
                    }

                }


                //Collection of Builds
                
                BuildCollection buildCollection = new BuildCollection();
                IEnumerable<BuildBO> buildList = buildCollection.GetRecords();

                //Collection of Site                 
                SiteCollection siteCollection = new SiteCollection();
                IEnumerable<SiteBO> siteList = siteCollection.GetRecords();

                //Collection of TestCaseList                
                TestCaseCollection testCaseCollection = new TestCaseCollection();
                IEnumerable<TestCaseBO> testCaseList = testCaseCollection.GetRecords();
                List<int> listTestCaseIds = testCaseCollection.GetRecords().Select(x => x.TestCaseId).ToList();

                //Collection of TestCaseRuleMapList                 
                TestCaseRuleMapCollection testCaseMapCollection = new TestCaseRuleMapCollection();
                IEnumerable<TestCaseRuleMapBO> testCaseRuleMapList = testCaseMapCollection.GetRecords();

                IEnumerable<TestCaseRuleMapBO> testCaseRuleMap = testCaseMapCollection.GetRecords();
                //List<int> listTestCaseIds  = testCaseCollection.GetRecords().Select(x=>x.TestCaseId).ToList();

                List<ViolationBO> violationBOLst = new List<ViolationBO>();

                //Collection of duplicate Rules
                IEnumerable<RuleDetailBO> DupRuleDetailIdLst = null;
                DupRuleDetailIdLst = (from i in ruleDetailsList
                                      where i.IsDuplicate == true
                                      select i).ToList();

                ruleDetailsList = ruleDetailsList.ToList();
                // Get already violations for duplicate rules
                var violationLstDup1 = (from b in buildList
                                        join s in siteList on b.SiteId equals s.SiteId
                                        join t in testCaseList on b.BuildId equals t.BuildId
                                        join tr in testCaseRuleMapList on t.TestCaseId equals tr.TestCaseId
                                        join r in ruleDetailsList on tr.RuleDetailId equals r.RuleDetailId
                                        join r1 in ruleDetailsList on r.RuleDetailId equals r1.DuplicateRuleDetailId
                                        join v in violationLst on tr.TRMappingId equals v.TRMappingId
                                        where listTestCaseIds.Contains(t.TestCaseId) && t.BuildId == buildId && t.IsActive == true
                                        select v).ToList();



                //update the TRMappingId
                violationLstDup1.Select(v1 => v1.TRMappingId = v1.DupTRMappingId).ToList();

                //List of RuleDetailName,RuleDetailDescription & TRMappingId To update the dupViol
                var violNameDespLst = (from tr1 in testCaseRuleMapList
                                       join v1 in violationLstDup1 on tr1.TRMappingId equals v1.DupTRMappingId
                                       //join rd in ruleDetailsList on tr1.RuleDetailId equals rd.RuleDetailId
                                       join rd in DupRuleDetailIdLst on tr1.RuleDetailId equals rd.RuleDetailId
                                       select new { ruleName = rd.RuleDetailName, ruleDesp = rd.RuleDetailDescription, TRMId = tr1.TRMappingId }).ToList();

                //Update the violNameDespLst with RuleName based on TRMappingId
                violationLstDup1.ToList().ForEach(d =>
                {
                
                    if (d.TRMappingId != 0)
                    {
                        d.ViolationName = violNameDespLst.Where(s => s.TRMId.Equals(d.TRMappingId)).First().ruleName;
                    }

                });

                //Insert the Duplicate set of records to the DB
                if (violationLstDup1 != null && violationLstDup1.Any())
                {
                    int noOfBatch = violationLstDup1.Count() > violationPerBatch ? violationLstDup1.Count() / violationPerBatch : 0;
                    ViolationCollection collectionObj1 = null;

                    for (int i = 0; i <= noOfBatch; i++)
                    {
                        int skip = noOfBatch > 0 && i > 0 ? i * violationPerBatch : 0;
                        collectionObj1 = new ViolationCollection();
                        IList<IViolation> violationList1 = noOfBatch > 0 ? GetViolationsList(violationLstDup1, skip, violationPerBatch) : violationLstDup1;

                        collectionObj1.CreateViolations(violationList1);
                        status = violationList1.Count;
                    }
                }

                //Method to update the Completed/Failure status to Violation_Log table
                updateViolationLog(objGlobalStatusList);
                objGlobalStatusList.Clear();

                //updateDecliendViolations(clientId, buildId, runId);

                #region TPLApproach

                //    var asyncTestCase = new Action<TestCaseBO>((testCase) =>
                //            {
                //                if (!testCase.Equals(null))
                //                {
                //                    ruleDetailsList = ruleDetailsForValidation.PrepareRulesToTest(buildId, testCase.TestCaseId);

                //                    if (!ruleDetailsList.Equals(null))
                //                    {
                //                        //Code to Bulk insert into Violation_Log Table
                //                        CreateViolationsLog(urlNames, testCase.TestCaseId, clientId, buildId, runId);
                //                        //code to Bulk insert into Violation_Log Table Ends here

                //                        Task[] tasks = new Task[urlNames.Count];

                //                        int violationLogId = 0;

                //                        try
                //                        {
                //                            bool isDone = false;
                //                            var asyncExecution = new Action<string>((urlString) =>
                //                            {

                //                                if (!urlString.Contains("https"))
                //                                {
                //                                    //Method to get ViolationLogId
                //                                    violationLogId = GetViolationLogId(urlString, testCase.TestCaseId, clientId, buildId, runId);
                //                                    isDone = ExecuteValidation(urlString, ruleDetailsList, ruleDetailsForValidation.TestCaseRuleMapList, clientId, buildId, runId, violationLogId);

                //                                    //Code to update the status in the violation Table                                                    
                //                                    //objStatusDictionay.Add(violationLogId, isDone);
                //                                }
                //                            });

                //                            int cnt = 0;

                //                            var threadCallBack = new Action(() =>
                //                            {
                //                                if (isDone)
                //                                {
                //                                    errorReport = (from violations in violationReport select violations).ToList();
                //                                }

                //                            });

                //                            //Begining and invoking async call.
                //                            foreach (string url in urlNames)
                //                            {

                //                                tasks[cnt] = Task.Factory.FromAsync(asyncExecution.BeginInvoke, asyncExecution.EndInvoke, url, asyncExecution);
                //                                //.ContinueWith(_ => threadCallBack());

                //                                cnt++;

                //                            }
                //                            Task.WaitAll(tasks);
                //                        }
                //                        finally
                //                        {
                //                            if (tasks.Count() > 0)
                //                            {
                //                                for (int i = 0; i < tasks.Length; i++)
                //                                {
                //                                    tasks[i].Dispose();
                //                                }
                //                            }
                //                        }

                //                    }
                //                }
                //            });

                //    var mainThreadCallBack = new Action(() =>
                //        {

                //        }
                //    );


                //    int mainCnt = 0;

                //    foreach (TestCaseBO testCaseBo in testcaseList)
                //    {
                //        mainTask[mainCnt] = Task.Factory.FromAsync(asyncTestCase.BeginInvoke, asyncTestCase.EndInvoke, testCaseBo, asyncTestCase);
                //        //.ContinueWith(_ => mainThreadCallBack());
                //        mainCnt++;
                //    }

                //    Task.WaitAll(mainTask);
                //}
                //finally
                //{
                //    if (mainTask.Count() > 0)
                //    {
                //        for (int i = 0; i < mainTask.Length; i++)
                //        {
                //            mainTask[i].Dispose();
                //        }
                //    }

                //}

                #endregion


                #region OldImplementation

                ////Dictionary<int, bool> objStatusDictionay = new Dictionary<int, bool>();

                ////foreach (TestCaseBO item in testcaseList)
                ////{
                ////    //Prepares rule details for list of Rules based on Build.  
                ////    ruleDetailsList = ruleDetailsForValidation.PrepareRulesToTest(buildId, item.TestCaseId);

                ////    //Method to insert Bulk Data to Violation_Log
                ////    CreateViolationsLog(urlNames, item.TestCaseId, clientId, buildId, runId);                    

                ////    foreach (string name in urlNames)
                ////    {
                ////        if (!name.Contains("https"))
                ////        {
                ////            try
                ////            {
                ////                //Method to fetch the ViolationLogId to insert the same to violations table
                ////                int violationLogId = GetViolationLogId(name, item.TestCaseId, clientId, buildId, runId);

                ////                bool isDone = ExecuteValidation(name, ruleDetailsList, ruleDetailsForValidation.TestCaseRuleMapList, clientId, buildId, runId, violationLogId);
                ////                status = 0;

                ////                //Method to update Violation_Log Table
                ////                if (isDone)
                ////                {                                    
                ////                    objStatusDictionay.Add(violationLogId, isDone);                                    
                ////                }

                ////            }
                ////            catch (Exception ex)
                ////            {
                ////                LogException.CatchExceptionTestService(name);
                ////            }
                ////        }
                ////    }
                ////} 
                //////Method to update the Completed/Failure status to Violation_Log table
                ////updateViolationLog(objStatusDictionay);

                #endregion
            }
            catch (TimeoutException timeOutEx)
            {
                throw timeOutEx;
               // LogException.CatchException(timeOutEx, "RuleValidator", "TestValidation");
            }
            catch (Exception generalEx)
            {
                throw generalEx;
                //LogException.CatchException(generalEx, "RuleValidator", "TestValidation");
            }
            finally
            {
                tc = new TestCaseCollection();
                ruleDetailsForValidation = new PrepareRules();
                testcaseList = null;
                ruleDetailsList = null;
            }

            return status;
        }

        #endregion Public Methods

        #region ::: Private Helpers :::

        //Generic Function to fetch 1 Lakh records
        private List<IViolation> GetViolationsList(IList<IViolation> violationList, int skip, int violationPerBatch)
        {
            List<IViolation> listOfViolations = null;
            listOfViolations = violationList.Skip(skip).Take(violationPerBatch).ToList();
            return listOfViolations;
        }

        private IList<IViolation> createViolations(List<ViolationBO> violationLog, RuleDetailBO ruleDetailUnderValidation,
              string urlName, int clientId, int buildId, int runId, int violationLogId, IEnumerable<RuleDetailBO> ruleDetailListToValidate)
        {
            IList<IViolation> violationLogToTable = new List<IViolation>();


            //Check if the ruleDetailsId is presnt in any of the ruleDetailListToValidate per TestCase
            var flag = ruleDetailListToValidate.Any(a => a.DuplicateRuleDetailId == ruleDetailUnderValidation.RuleDetailId);

            try
            {
                if (violationLog != null && violationLog.Count > 0)
                {
                    foreach (IViolation log in violationLog)
                    {
                        //Save violation databoo to DB
                        log.SourceURL = urlName;

                        if (flag)
                        {
                            // Updated only for Duplicate Rules 
                            if (ruleDetailUnderValidation.DuplicateRuleDetailId != 0)
                            {
                                //log.ViolationName = ruleDetailUnderValidation.RuleDetailName;
                                log.DupTRMappingId = testCaseRuleDetailMapping.First<TestCaseRuleMapBO>(x => x.RuleDetailId == ruleDetailUnderValidation.DuplicateRuleDetailId).TRMappingId;
                            }
                        }

                        log.ViolationName = ruleDetailUnderValidation.RuleDetailName;
                        log.TRMappingId = testCaseRuleDetailMapping.First<TestCaseRuleMapBO>(x => x.RuleDetailId == ruleDetailUnderValidation.RuleDetailId).TRMappingId;
                        log.Recomandation = ruleDetailUnderValidation.StepsToPerform;
                        log.ClientId = clientId;
                        log.BuildId = buildId;
                        log.RunId = runId;
                        log.Status = resourceManager.GetString("StatusFailed");
                        log.violationLogId = violationLogId;
                        log.CreateDate = System.DateTime.Now;
                        log.UpdateDate = System.DateTime.Now;
                        violationLogToTable.Add(log);
                    }
                }
                return violationLogToTable;
            }
            catch (Exception exp)
            {
                LogException.CatchException(exp, "RuleValidator", "createViolations");
                return violationLogToTable;
            }
            finally
            {
                violationLogToTable = new List<IViolation>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="violationLog"></param>
        /// <param name="ruleDetailUnderValidation"></param>
        /// <param name="urlName"></param>
        /// <returns></returns>

        private bool createContentViolationReport(List<ViolationBO> violationLog, RuleDetailBO ruleDetailUnderValidation,
              string urlName, int clientId, int buildId, int runId)
        {
            try
            {
                if (violationLog != null && violationLog.Any())
                {
                    foreach (ViolationBO log in violationLog)
                    {
                        //Save violation databoo to DB
                        log.SourceURL = urlName;
                        log.ViolationName = ruleDetailUnderValidation.RuleDetailName;
                        //log.ViolationDescription = ruleDetailUnderValidation.RuleDetailDescription;
                        log.TRMappingId = testCaseRuleDetailMapping.First<TestCaseRuleMapBO>(x => x.RuleDetailId == ruleDetailUnderValidation.RuleDetailId).TRMappingId;
                        log.Recomandation = ruleDetailUnderValidation.StepsToPerform;
                        log.ClientId = clientId;
                        log.BuildId = buildId;
                        log.RunId = runId;
                        log.Status = resourceManager.GetString("StatusFailed");
                        violationReport.Add(log);
                        //Save violation data to DB
                        log.CreateViolation();
                    }

                }

            }
            catch (Exception exp)
            {
                LogException.CatchException(exp, "RuleValidator", "createContentViolationReport");
            }

            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleDetailUnderValidation"></param>
        /// <param name="node"></param>
        /// <param name="urlName"></param>
        /// <returns></returns>
        private bool createNodeViolationReport(RuleDetailBO ruleDetailUnderValidation, HtmlNode node,
             string urlName, int clientId, int buildId, int runId, bool validationPassed)
        {
            ViolationBO violation = new ViolationBO();

            try
            {
                if (!validationPassed)
                {
                    violation.SourceURL = urlName;
                    violation.ViolationName = ruleDetailUnderValidation.RuleDetailName;
                    violation.ViolationDescription = ruleDetailUnderValidation.RuleDetailDescription;
                    violation.SourceElement = node.OuterHtml;
                    violation.SourceLineNumber = node.Line;
                    violation.Recomandation = ruleDetailUnderValidation.StepsToPerform;
                    violation.TRMappingId = testCaseRuleDetailMapping.First<TestCaseRuleMapBO>(x => x.RuleDetailId == ruleDetailUnderValidation.RuleDetailId).TRMappingId;
                    violation.ClientId = clientId;
                    violation.BuildId = buildId;
                    violation.RunId = runId;
                    //violation.Status = StatusValues.Failed;
                    violation.Status = resourceManager.GetString("StatusFailed");
                    //Save violation data to DB
                    violation.CreateViolation();

                    violationReport.Add(violation);
                }
            }
            catch (Exception exp)
            {
                LogException.CatchException(exp, "RuleValidator", "createNodeViolationReport");
            }
            finally
            {
                violation = new ViolationBO();
            }

            return true;
        }


        /// <summary>
        /// Gets the methodinfo object to run the validation taking method name from ruledetail under validation as parameter.
        /// </summary>
        /// <param name="assemblyInfo"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        private MethodInfo getMethodToInvoke(Type assemblyInfo, string methodName)
        {
            MethodInfo[] methods = assemblyInfo.GetMethods();
            MethodInfo methodToInvoke = null;

            try
            {
                foreach (MethodInfo method in methods)
                {
                    if (method.Name.ToLower().Trim() == methodName.ToLower())
                    {
                        methodToInvoke = method;
                        break;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException.CatchException(exp, "RuleValidator", "getMethodToInvoke");
            }
            finally
            {
                methods = null;
                //methodToInvoke = null;
            }

            return methodToInvoke;
        }

        /// <summary>
        /// update decliend violations accross runs for a client and build
        /// </summary>
        public void updateDecliendViolations(int clientId, int buildId, int runId)
        {
            ViolationCollection violationCol = new ViolationCollection();
            List<ViolationBO> currentViolationList = new List<ViolationBO>();
            List<ViolationBO> previousRunResult = new List<ViolationBO>();
            List<ViolationBO> violationObj = new List<ViolationBO>();

            try
            {
                currentViolationList = violationCol.GetSpecificRecords(clientId, buildId, runId).ToList<ViolationBO>();

                int previousRunID = getPreviousRunId(violationCol, clientId, buildId, runId);

                if (previousRunID > 0)
                {
                    previousRunResult = violationCol.GetSpecificRecords(clientId, buildId, previousRunID).Where(x => x.Status == "D").ToList<ViolationBO>();

                    if (previousRunResult != null && previousRunResult.Any())
                    {
                        violationObj = (from item in previousRunResult
                                        from cv in currentViolationList
                                        where cv.SourceURL.Equals(item.SourceURL)
                                        && cv.SourceElement.Equals(item.SourceElement)
                                        && cv.ViolationName.Equals(item.ViolationName)
                                        && cv.Status.ToUpper().Equals("F")
                                        select new { cv }.cv).ToList();

                        if (violationObj != null)
                        {
                            violationObj.ToList().ForEach(obj =>
                            {
                                obj.Status = "D";
                                violationCol.UpdateDataSingle(obj);
                            });


                            //foreach (ViolationBO obj in violationObj)
                            //{
                            //    obj.Status = "D";
                            //    violationCol.UpdateDataSingle(obj);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleValidator", "updateDecliendViolations");
            }
            finally
            {
                violationCol = new ViolationCollection();
                currentViolationList = new List<ViolationBO>();
                previousRunResult = new List<ViolationBO>();
                violationObj = new List<ViolationBO>();
            }

        }

        private int getPreviousRunId(ViolationCollection violationCol, int clientId, int buildId, int runId)
        {
            int previousRunID;
            try
            {
                previousRunID = violationCol.GetRecords(clientId).Where(x => x.BuildId == buildId).OrderByDescending(x => x.RunId).Select(y => y.RunId).Distinct().Skip(1).First();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleValidator", "getPreviousRunId");
                return 0;
            }
            return previousRunID;
        }

        /// <summary>
        /// Bulk insert data to violation_Log table
        /// </summary>
        private void CreateViolationsLog(List<string> urlNames, int testCaseId, int clientId, int buildId, int runId)
        {
            IList<IViolationLog> violationLogsToTable = new List<IViolationLog>();
            ViolationLogCollection collection = new ViolationLogCollection();

            try
            {
                if (urlNames != null && urlNames.Any())
                {
                    violationLogsToTable = (from url in urlNames
                                            select new ViolationLogBO()
                                            {
                                                SourceURL = url,
                                                TestCaseId = testCaseId,
                                                ClientId = clientId,
                                                BuildId = buildId,
                                                RunId = runId,
                                                Status = 'N',
                                                CreateDate = System.DateTime.Now,
                                                UpdateDate = System.DateTime.Now
                                            }).Cast<IViolationLog>().ToList();


                    collection.CreateViolationsLog(violationLogsToTable);
                }
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleValidator", "CreateViolationsLog");
            }
            finally
            {
                violationLogsToTable = new List<IViolationLog>();
                collection = new ViolationLogCollection();
            }
        }

        /// <summary>
        /// Update status in Violation_Log table
        /// </summary>
        private void updateViolationLog(List<ViolationLogData> objStatusList)
        {
            ViolationLogCollection collection = new ViolationLogCollection();
            Dictionary<int, char> objStatusDictionay = new Dictionary<int, char>();
            bool isUpdate = false;
            try
            {
                List<ViolationLogData> failedLst = (from item in objStatusList
                                                    where !(item.status)
                                                    select new ViolationLogData
                                                    {
                                                        invokedMethodName = item.invokedMethodName,
                                                        sourceURL = item.sourceURL,
                                                        violationLogId = item.violationLogId,
                                                        status = item.status
                                                    }).ToList();


                var allLogLst = objStatusList.Select(x => new { x.violationLogId }).Distinct();

                allLogLst.ToList().ForEach(d =>
                {
                    if (!objStatusDictionay.ContainsKey(d.violationLogId))
                    {
                        if ((from lst in failedLst
                             where d.violationLogId == lst.violationLogId
                             select lst).Count() >= 1)
                            objStatusDictionay.Add(d.violationLogId, 'F');
                        else
                            objStatusDictionay.Add(d.violationLogId, 'C');
                    }
                });

                isUpdate = collection.updateViolationLog(objStatusDictionay);

            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleValidator", "updateViolationLog");
            }
        }

        /// <summary>
        /// Method to fetch ViolationLogId
        /// </summary>
        private int GetViolationLogId(string urlName, int testCaseId, int clientId, int buildId, int runId)
        {
            int violationLogId = 0;
            ViolationLogCollection collectionObj = new ViolationLogCollection();

            try
            {
                violationLogId = collectionObj.GetViolationLogId(urlName, testCaseId, clientId, buildId, runId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleValidator", "GetViolationLogId");
            }
            return violationLogId;
        }


        /// <summary>
        /// Update status in Violation_Log table with Inprogress
        /// </summary>
        private void UpdateViolationsLogStatus(int violationLogId)
        {
            ViolationLogCollection collection = new ViolationLogCollection();
            bool isUpdate = false;
            //Set status to InProgress
            string statusVal = "I";

            isUpdate = collection.UpdateViolationLogWithStatus(violationLogId, statusVal);

        }


        #endregion ::: Private Helpers :::

    }
}
