﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;
using HtmlAgilityPack;
using System.Resources;
using AccessibilityAnalyser.Common;
using System.Text.RegularExpressions;

namespace AccessibilityAnalyser.RuleEngine
{
    public class RuleEngineHelper
    {
        static ResourceManager resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);

        public static List<ViolationBO> CheckForLogicalTabOrder(List<ViolationBO> violationList, List<HtmlNode> nodeList,
                                                                string checkTagOrderElement = null, bool userNavWWithinContainer = false)
        {
            int startIndex;
            List<HtmlNode> tabIndexNodes = new List<HtmlNode>();
            List<int> tabIndexList = new List<int>();
            List<HtmlNode> allNodesWithNonNumericTabIndex = new List<HtmlNode>();
            List<int> violationIndex = new List<int>();
            List<HtmlNode> allChildTags = new List<HtmlNode>();


            if (checkTagOrderElement != null)
            {
                allChildTags = (from i in nodeList
                                where i.Name.ToLower().Trim().Equals(checkTagOrderElement)
                                select i).ToList();
            }
            else
            {
                allChildTags.AddRange(nodeList);
            }


            List<HtmlNode> allNodesWithTabIndex = (from d in allChildTags
                                                   where d.Attributes["tabindex"] != null && !string.IsNullOrEmpty(d.Attributes["tabindex"].Value)
                                                   select d).ToList();

            // Get all nodes with numeric tab index value
            tabIndexNodes.AddRange((from i in allNodesWithTabIndex
                                    where (AccessibilityAnalyser.Common.Helper.IsPositiveNum(i.Attributes["tabindex"].Value))
                                    select i).ToList());

            // Get all node values with numeric tab index value
            tabIndexList.AddRange(tabIndexNodes.Select(o => Convert.ToInt32(o.Attributes["tabindex"].Value)));

            // Get all nodes with non numeric tab index value
            allNodesWithNonNumericTabIndex = (from i in allNodesWithTabIndex
                                              where (!AccessibilityAnalyser.Common.Helper.IsNumeric(i.Attributes["tabindex"].Value))
                                              select i).ToList();

            // Generate violations for all nono numerice tab index values
            violationList = (from item in allNodesWithNonNumericTabIndex
                             select new ViolationBO
                             {
                                 SourceElement = item.OuterHtml,
                                 SourceLineNumber = item.Line,
                                 //ViolationDescription = resourceManager.GetString("LogicalTabOrder")
                                 ViolationDescription = checkTagOrderElement == null ? resourceManager.GetString("LogicalTabOrder") : resourceManager.GetString("LogicalTabOrderSubMenu")
                             }).ToList();

            // Generate violation for all nodes without tab index. E.g li / div
            if ((checkTagOrderElement != null || userNavWWithinContainer) && tabIndexList.Count > 0)
            {
                violationList.AddRange((from d in allChildTags
                                        where d.Attributes["tabindex"] == null || string.IsNullOrEmpty(d.Attributes["tabindex"].Value)
                                        select new ViolationBO
                                        {
                                            SourceElement = d.OuterHtml,
                                            SourceLineNumber = d.Line,
                                            //ViolationDescription = resourceManager.GetString("LogicalTabOrder")
                                            ViolationDescription = checkTagOrderElement == null ? resourceManager.GetString("LogicalTabOrder") : resourceManager.GetString("LogicalTabOrderSubMenu")
                                        }).ToList());
            }

            if (tabIndexNodes != null && tabIndexNodes.Count > 0 && tabIndexList.Count > 0)
            {
                for (startIndex = 0; startIndex < tabIndexList.Count; startIndex++)
                {
                    // Compare the tabindex with subsequent indexes
                    for (int nextIndex = startIndex + 1; nextIndex < tabIndexList.Count; nextIndex++)
                    {
                        if (tabIndexList[nextIndex] <= tabIndexList[startIndex])
                        {
                            if (!violationIndex.Contains(tabIndexList[nextIndex]))
                            {
                                violationIndex.Add(tabIndexList[nextIndex]);
                                ViolationBO violationBO = new ViolationBO
                                {
                                    SourceElement = tabIndexNodes[nextIndex].OuterHtml,
                                    SourceLineNumber = tabIndexNodes[nextIndex].Line,
                                    //ViolationDescription = resourceManager.GetString("LogicalTabOrder")
                                    ViolationDescription = checkTagOrderElement == null ? resourceManager.GetString("LogicalTabOrder") : resourceManager.GetString("LogicalTabOrderSubMenu")
                                };
                                violationList.Add(violationBO);
                            }
                        }
                    }
                }
            }

            return violationList;
        }

        /// <summary>
        ///  Method to fill data to Dictionary Object
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>Dictionary Object</returns>
        public static Dictionary<int, string> FillData(HtmlNodeCollection objNodeColl)
        {
            Dictionary<int, string> objDictData = new Dictionary<int, string>();
            int counter = 1;

            if (objNodeColl != null)
            {
                foreach (HtmlNode node in objNodeColl)
                {
                    objDictData.Add(counter++, node.InnerText.ToString().ToLower().Trim());
                }
            }
            return objDictData;
        }

        /// <summary>
        ///  Method to Fetch the header(th) nodes from the HtmlTable
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>HtmlNodeCollection Object</returns>
        public static HtmlNodeCollection TableHeaderCollection(HtmlNode tableNode)
        {
            HtmlNodeCollection objNodeCol = null;

            if (tableNode.SelectNodes("." + resourceManager.GetString("THeadTrThTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("THeadTrThTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("THeadTrTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("THeadTrTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("THeadTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("THeadTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("TrThTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("TrThTag"));
            }

            return objNodeCol;
        }

        /// <summary>
        ///  Method to Fetch the Body(tr) nodes from the HtmlTable
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>HtmlNodeCollection Object</returns>
        public static HtmlNodeCollection TableBodyCollection(HtmlNode tableNode)
        {
            HtmlNodeCollection objNodeCol = null;

            if (tableNode.SelectNodes("." + resourceManager.GetString("TBodyTrTdTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("TBodyTrTdTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("TBodyTdTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("TBodyTdTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("TBodyTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("TBodyTag"));
            }
            else if (tableNode.SelectNodes("." + resourceManager.GetString("TrTdTag")) != null)
            {
                objNodeCol = tableNode.SelectNodes("." + resourceManager.GetString("TrTdTag"));
            }

            return objNodeCol;
        }

        /// <summary>
        ///  Method to return a Dictionary contisting of style-property : value
        /// </summary>
        /// <param name="style"></param>
        /// <returns>Dictionary <property,value> </returns>
        public static Dictionary<string, string> GetStyleProperties(string style)
        {
            Dictionary<string, string> styleProperties = new Dictionary<string, string>();
            styleProperties = style.Split(';').Select(part => part.Split(':')).Where(part => part.Length == 2).GroupBy(pair => pair[0].ToString()).Select(group => group.First()).ToDictionary(sp => sp[0], sp => sp[1]);
            return styleProperties;
        }

        /// <summary>
        /// Checks if the input url is a Valid url
        /// </summary>
        /// <param name="urls"></param>
        /// <returns></returns>
        public static string ValidateUrl(string urls)
        {
            string validUrls = string.Empty;
            string regex = @"^((http)://|(https)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$";
            Match urlMatch = Regex.Match(urls, regex);
            if (urlMatch.Success)
            {
                validUrls = urls;
            }
            return validUrls;
        }

        
        /// <summary>
        /// Gives base address for the given url
        /// </summary>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public static string GetBaseSiteName(string siteName)
        {
            string baseSiteAddress = string.Empty;
            try
            {
                string[] splitAddress = siteName.Split('/');

                baseSiteAddress = splitAddress[2];

                return (!string.IsNullOrEmpty(baseSiteAddress)) ? baseSiteAddress : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gives All NonDecorative Images for List of Images
        /// </summary>
        /// <param name="lstNodes"></param>
        /// <param name="classProp"></param>
        /// <returns>List<HtmlNode></returns>
        /// <returns>Dictionary<string, string> classProp></returns>
        public static List<HtmlNode> GetAllNonDecorativeImg(List<HtmlNode> lstNodes, Dictionary<string, string> classProp)
        {
            List<HtmlNode> lstAllNonDecImgs = new List<HtmlNode>();
            try
            {
                //Add those Images or its parent or any of its Ancestor are not place within <noscript> &  src != null & src has value
                var lstimgNode = (from i in lstNodes
                                  where i.ParentNode.Name.ToLower().Trim() == "noscript" || i.Attributes["src"] == null || string.IsNullOrEmpty(i.Attributes["src"].Value.Trim()) || i.Attributes["src"].Value.ToLower().Trim() == "#"
                                  || i.Ancestors().Where(j => j.Name.ToLower().Trim() == "noscript").Count() > 0
                                  select i).ToList();

                //2.Check if the Current Node or it's Parent has in-Line style as- display : none || visibility : hidden
                lstimgNode.AddRange(from item in lstNodes
                                    where (item.Attributes["style"] != null && !string.IsNullOrEmpty(item.Attributes["style"].Value.ToLower().Trim())
                                     || item.ParentNode.Attributes["style"] != null && !string.IsNullOrEmpty(item.ParentNode.Attributes["style"].Value.ToLower().Trim()))

                                    //let Style = item.Attributes["style"] != null ? item.Attributes["style"].Value.ToLower().Trim() : item.ParentNode.Attributes["style"].Value.ToLower().Trim()

                                    let Style = item.Attributes["style"] != null ? (!string.IsNullOrEmpty(item.Attributes["style"].Value.Trim()) ? item.Attributes["style"].Value.ToLower().Trim() : item.ParentNode.Attributes["style"].Value.ToLower().Trim()) : string.Empty
                                    where !string.IsNullOrEmpty(Style)
                                    let key = GetStyleProperties(Style).FirstOrDefault(x => x.Key.ToLower().Trim() == "display" || x.Key.ToLower().Trim() == "visibility")

                                    let VALUE = key.Value != null ? key.Value : string.Empty

                                    where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || (VALUE.ToLower().Trim() == "hidden")))
                                    select item);

                //3.Check from the remaining Nodes if any of their Ancestors have in-Line style "display:none" ? visibility : hidden  Add to the List  
                lstimgNode.AddRange(from item in lstNodes.Where(i => i.Ancestors().Count() > 0)
                                    where (item.Ancestors().Where(j => j.Attributes.Any(k => k.Name.ToLower().Trim() == "style")).Count() > 0)
                                    let x = item.Ancestors().SelectMany(j => j.Attributes.Where(k => k.Name.ToLower().Trim() == "style"))
                                    let key = RuleEngineHelper.GetStyleProperties(x.FirstOrDefault(i => i.Name.ToLower() == "style").Value).FirstOrDefault(X => X.Key.ToLower().Trim() == "display" || X.Key.ToLower().Trim() == "visibility")

                                    let VALUE = key.Value != null ? key.Value : string.Empty

                                    where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || (VALUE.ToLower().Trim() == "hidden")))
                                    select item);


                if (lstimgNode.Count > 0)
                {
                    lstNodes.RemoveAll(i => lstimgNode.Contains(i));
                    lstimgNode.Clear();
                }
                //4.Check if the Current Node or it's Parent has Attr = Class  with display : none || visibility : hidden
                lstimgNode = (from item in lstNodes
                              where (item.Attributes["class"] != null && !string.IsNullOrEmpty(item.Attributes["class"].Value.ToLower().Trim())
                               || item.ParentNode.Attributes["class"] != null && !string.IsNullOrEmpty(item.ParentNode.Attributes["class"].Value.ToLower().Trim()))

                              //let className = item.Attributes["class"] != null ? item.Attributes["class"].Value.ToLower().Trim() : item.ParentNode.Attributes["class"].Value.ToLower().Trim()
                              let className = item.Attributes["class"] != null ? (!string.IsNullOrEmpty(item.Attributes["class"].Value.Trim()) ? item.Attributes["class"].Value.ToLower().Trim() : item.ParentNode.Attributes["class"].Value.ToLower().Trim()) : string.Empty
                              where !string.IsNullOrEmpty(className)
                              let Style = classProp.Where(i => i.Key == (resourceManager.GetString("Dot")) + className)
                              from j in Style
                              let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower().Trim()).FirstOrDefault(x => x.Key.ToLower().Trim() == "display" || x.Key.ToLower().Trim() == "visibility")

                              let VALUE = key.Value != null ? key.Value : string.Empty

                              where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || VALUE.ToLower().Trim() == "hidden"))
                              select item).ToList();

                //5.Check from the remaining Nodes if any of their Ancestors have Attr = Class with "display:none" ? visibility : hidden  Add to the List  
                lstimgNode.AddRange(from item in lstNodes.Where(i => i.Ancestors().Count() > 0)
                                    where (item.Ancestors().Where(j => j.Attributes.Any(k => k.Name.ToLower().Trim() == "class")).Count() > 0)

                                    from v in item.Ancestors()
                                    let tt = v.Attributes["class"] != null ? v.Attributes["class"].Value.ToLower().Trim() : string.Empty

                                    let Style = classProp.Where(i => i.Key == (resourceManager.GetString("Dot")) + tt)
                                    from j in Style
                                    let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower().Trim()).FirstOrDefault(k => k.Key.ToLower().Trim() == "display" || k.Key.ToLower().Trim() == "visibility")

                                    let VALUE = key.Value != null ? key.Value : string.Empty
                                    where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || VALUE.ToLower().Trim() == "hidden"))
                                    select item);


                //1.Check If the Images have title associated with it. OR
                //2.Check where Images or its parents or Ancestors have  Onclick associated with it. 
                //3.Check if the parent is <a> with href redirect to some url
                //4.Check If <input type="image" has any Ancestor  as <form> with action != null Add it to the list
                //5.Check if any <img usmap="#Maptag" i.e if any img tag has any attr of usemap with some value.
                var lstImages = (from i in lstNodes
                                 where ((i.Attributes["title"] != null && !string.IsNullOrEmpty(i.Attributes["title"].Value)) ||
                                         i.Attributes.Contains(resourceManager.GetString("OnClick")) || i.ParentNode.Attributes.Contains(resourceManager.GetString("OnClick")) ||
                                         (i.ParentNode.Name.ToLower() == "a" && i.ParentNode.Attributes["href"] != null &&
                                         (i.ParentNode.Attributes["href"].Value.ToLower().Trim() != "#" && !string.IsNullOrEmpty(i.ParentNode.Attributes["href"].Value))) ||

                                         (i.Ancestors().Where(x => x.Name.ToLower().Trim() == "a" && x.Attributes["href"] != null && !string.IsNullOrEmpty(x.Attributes["href"].Value))).Count() > 0

                                        || i.ParentNode.ChildNodes.Where(j => j.Name.ToLower() == "form" && !string.IsNullOrEmpty(j.Attributes["action"].Value.Trim())).Count() > 0
                                        || i.Ancestors().Where(j => j.Attributes.Any(k => k.Name.ToLower().Trim() == "onclick")).Count() > 0
                                        || i.Attributes["usemap"] != null && !string.IsNullOrEmpty(i.Attributes["usemap"].Value.Trim())
                                       )
                                 select i).ToList();

                lstAllNonDecImgs = lstImages;
                //return lstImages;
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "RuleEngineHelper", "GetAllNonDecorativeImg");

            }
            return lstAllNonDecImgs;
        }


        /// <summary>
        /// Gives All Valid Nodes
        /// To verify that nodes do not have display : none 
        /// </summary>
        /// <param name="lstNodes"></param>
        /// <returns>List<HtmlNode></returns>
        public static List<HtmlNode> getAllValidNodes(List<HtmlNode> lstNodes, Dictionary<string, string> classProp)
        {
            var lstAllNodes = lstNodes;
            try
            {
                //1.Check which of the Nodes and its Parents have InLine style "display:none" ? Add to the List
                var lstOfCurrentNodes = (from item in lstAllNodes
                                         where (item.Attributes["style"] != null && !string.IsNullOrEmpty(item.Attributes["style"].Value.ToLower().Trim())
                                          || item.ParentNode.Attributes["style"] != null && !string.IsNullOrEmpty(item.ParentNode.Attributes["style"].Value.ToLower().Trim()))

                                         //let Style = item.Attributes["style"] != null ? item.Attributes["style"].Value.ToLower().Trim() : item.ParentNode.Attributes["style"].Value.ToLower().Trim() 
                                         let Style = item.Attributes["style"] != null ? (!string.IsNullOrEmpty(item.Attributes["style"].Value.Trim()) ? item.Attributes["style"].Value.ToLower().Trim() : item.ParentNode.Attributes["style"].Value.ToLower().Trim()) : string.Empty
                                         where !string.IsNullOrEmpty(Style)
                                         let key = RuleEngineHelper.GetStyleProperties(Style).FirstOrDefault(x => x.Key.ToLower().Trim() == "display" || x.Key.ToLower().Trim() == "visibility")
                                         let VALUE = key.Value != null ? key.Value : string.Empty

                                         where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || VALUE.ToLower().Trim() == "hidden"))

                                         //where (!string.IsNullOrEmpty(key.Value) && (key.Value.ToLower().Trim() == "none" || key.Value.ToLower().Trim() == "hidden"))
                                         select item).ToList();

                //2.Check from the remaining Nodes if any of their Ancestors have style "display:none" ? Add to the List 
                lstOfCurrentNodes.AddRange(from item in lstAllNodes.Where(i => i.Ancestors().Count() > 0)
                                           where (item.Ancestors().Where(j => j.Attributes.Any(k => k.Name.ToLower().Trim() == "style")).Count() > 0)
                                           let x = item.Ancestors().SelectMany(j => j.Attributes.Where(k => k.Name.ToLower().Trim() == "style"))

                                           let key = RuleEngineHelper.GetStyleProperties(x.FirstOrDefault(i => i.Name.ToLower() == "style").Value).FirstOrDefault(X => X.Key.ToLower().Trim() == "display" || X.Key.ToLower().Trim() == "visibility")

                                           let VALUE = key.Value != null ? key.Value : string.Empty

                                           where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || VALUE.ToLower().Trim() == "hidden"))

                                           select item);
                //Now Remove the Nodes 
                if (lstOfCurrentNodes.Count > 0)
                {
                    //Remove the Nodes which have style "display:none"
                    lstAllNodes.RemoveAll(i => lstOfCurrentNodes.Contains(i));
                    //Now clear the List
                    lstOfCurrentNodes.Clear();
                }

                //To check for Nodes with Alt = class and display : none
                //3. Check for the Nodes or its parents have any Attr with name Class and display : none
                lstOfCurrentNodes = (from item in lstAllNodes
                                     where (item.Attributes["class"] != null && !string.IsNullOrEmpty(item.Attributes["class"].Value.ToLower().Trim())
                                      || item.ParentNode.Attributes["class"] != null && !string.IsNullOrEmpty(item.ParentNode.Attributes["class"].Value.ToLower().Trim()))

                                     //let className = item.Attributes["class"] != null ? item.Attributes["class"].Value.ToLower().Trim() : item.ParentNode.Attributes["class"].Value.ToLower().Trim()

                                     let className = item.Attributes["class"] != null ? (!string.IsNullOrEmpty(item.Attributes["class"].Value.Trim()) ? item.Attributes["class"].Value.ToLower().Trim() : item.ParentNode.Attributes["class"].Value.ToLower().Trim()) : string.Empty
                                     where !string.IsNullOrEmpty(className)
                                     let Style = classProp.Where(i => i.Key == (resourceManager.GetString("Dot")) + className)

                                     from j in Style

                                     let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower().Trim()).FirstOrDefault(x => x.Key.ToLower().Trim() == "display" || x.Key.ToLower().Trim() == "visibility")
                                     let VALUE = key.Value != null ? key.Value : string.Empty
                                     where (!string.IsNullOrEmpty(VALUE) && (VALUE.ToLower().Trim() == "none" || VALUE.ToLower().Trim() == "hidden"))
                                     //where (!string.IsNullOrEmpty(key.Value) && (key.Value.ToLower().Trim() == "none" || key.Value.ToLower().Trim() == "hidden"))
                                     select item).ToList();


                //4.Check from the remaining Nodes if any of their Ancestors have Attr as Class and "display:none" ? Add to the List 

                lstOfCurrentNodes.AddRange(from item in lstAllNodes.Where(i => i.Ancestors().Count() > 0)
                                           where (item.Ancestors().Where(j => j.Attributes.Any(k => k.Name.ToLower().Trim() == "class")).Count() > 0)

                                           from v in item.Ancestors()
                                           let tt = v.Attributes["class"] != null ? v.Attributes["class"].Value.ToLower().Trim() : string.Empty

                                           where !string.IsNullOrEmpty(tt)

                                           let Style = classProp.Where(i => i.Key == (resourceManager.GetString("Dot")) + tt)
                                           from j in Style
                                           let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower().Trim()).FirstOrDefault(k => k.Key.ToLower().Trim() == "display" || k.Key.ToLower().Trim() == "visibility")
                                           where (!string.IsNullOrEmpty(key.Value) && (key.Value.ToLower().Trim() == "none" || key.Value.ToLower().Trim() == "hidden"))
                                           select item);



                //Now Remove the Nodes 
                if (lstOfCurrentNodes.Count > 0)
                {
                    lstAllNodes.RemoveAll(i => lstOfCurrentNodes.Contains(i));
                }

            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "RuleEngineHelper", "getAllValidNodes");
            }
            return lstAllNodes;
        }

        public static string ValidateAnimatedImageUrl(string urls, string baseAddress)
        {
            string validUrls = string.Empty;
            string regex = @"^((http)://|(https)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$";
            Match urlMatch = Regex.Match(urls, regex);

            if (urlMatch.Success)
            {
                validUrls = urls;
            }
            else
            {
                if (urls.Contains("/") && urls.IndexOf("/") == 0)
                {
                    if (!string.IsNullOrEmpty(baseAddress))
                    {
                        validUrls = "http://" + baseAddress + urls;
                    }
                }
                else
                {
                    validUrls = string.Empty;
                }
            }
            return validUrls;
        }
    }
}
