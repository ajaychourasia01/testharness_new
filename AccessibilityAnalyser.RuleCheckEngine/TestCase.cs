﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.RuleEngine
{
    public class TestCase
    {
        public int TestCaseId { get; set; }
        public string TestCaseName { get; set; }
        public string TestCaseDescription { get; set; }
        public int BuildId { get; set; }

        public bool IsActive
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public List<RuleDetailBO> ruleDetails;
        public List<TestCaseRuleMapBO> testrulemapDetails;
        public ViolationCollection Violation;

        public TestCase()
        {
            ruleDetails = new List<RuleDetailBO>();
            testrulemapDetails = new List<TestCaseRuleMapBO>();
            Violation = new ViolationCollection(); 
        }

        public void AddRuleDetail(RuleDetailBO ruleDetailTemp, int runId, int buildId)
        {
            RuleDetailBO ruleDetail = new RuleDetailBO
            {
                RuleDetailId = ruleDetailTemp.RuleDetailId,
                RuleDetailName = ruleDetailTemp.RuleDetailName,
                RuleDetailDescription = ruleDetailTemp.RuleDetailDescription,
                StepsToPerform = ruleDetailTemp.StepsToPerform,
                MethodToInvoke = ruleDetailTemp.MethodToInvoke,
                RuleId = ruleDetailTemp.RuleId,
                ElementToInspect = ruleDetailTemp.ElementToInspect,
                ElementType = ruleDetailTemp.ElementType,
                IsAutomated = ruleDetailTemp.IsAutomated,
                IsActive = ruleDetailTemp.IsActive,
                CreateDate = ruleDetailTemp.CreateDate,
                CreateBy = ruleDetailTemp.CreateBy,
                UpdateDate = ruleDetailTemp.UpdateDate,
                UpdateBy = ruleDetailTemp.UpdateBy,
                ViolationsCount = Violation.GetViolationsCountPerRule(buildId,runId,ruleDetailTemp.RuleDetailId,"Total"),
                AcceptedViolationsCount = Violation.GetViolationsCountPerRule(buildId,runId,ruleDetailTemp.RuleDetailId,"Accepted"),
                DeclinedViolationsCount = Violation.GetViolationsCountPerRule(buildId, runId, ruleDetailTemp.RuleDetailId, "Declined"),
            };

            ruleDetails.Add(ruleDetail);
        }

        public void AddRulePriority(TestCaseRuleMapBO TestRuleMapTemp)
        {
            TestCaseRuleMapBO testrulemapDetail = new TestCaseRuleMapBO
            {
                Priority = TestRuleMapTemp.Priority,
                RuleDetailId = TestRuleMapTemp.RuleDetailId,
                TestCaseId = TestRuleMapTemp.TestCaseId
            };

            testrulemapDetails.Add(testrulemapDetail);
        }
    }
}
