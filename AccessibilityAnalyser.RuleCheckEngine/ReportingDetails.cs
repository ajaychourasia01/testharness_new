﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.RuleEngine
{
    public class ReportingDetails
    {

        public ReportingDetails()
        {
            
        }

        public ReportingDetails(int buildId, int runId, string runName, int failedViolationsCount, int totalViolationsCount, int failCount, double failParcentage, int totalCount, DateTime? BuildCreatedDate, DateTime? RunCreatedDate, string buildName = null)
        {
            this.BuildId = buildId;
            this.RunId = runId;
            this.FailParcentage = failParcentage;
            this.BuildName = buildName;
            this.RunName = runName;
            this.TotalCount = totalCount;
            this.FailCount = failCount;
            this.FailedViolationsCount = failedViolationsCount;
            this.TotalViolationsCount = totalViolationsCount;
            
            if (BuildCreatedDate.HasValue)
            {
                this.BuildCreatedDate = Convert.ToDateTime(BuildCreatedDate);
            }

            if (RunCreatedDate.HasValue)
            {
                this.RunCreatedDate = Convert.ToDateTime(RunCreatedDate);
            }
        }

        public int BuildId
        {
            get;
            set;
        }

        public int FailCount
        {
            get;
            set;
        }

        public int FailedViolationsCount
        {
            get;
            set;
        }

        public int TotalViolationsCount
        {
            get;
            set;
        }

        public int RunId
        {
            get;
            set;
        }

        public int TotalCount
        {
            get;
            set;
        }

        public string RunName
        {
            get;
            set;
        }

        public string BuildName
        {
            get;
            set;
        }

        public double FailParcentage
        {
            get;
            set;
        }

        public DateTime BuildCreatedDate
        {
            get;
            set;
        }

        public Nullable<DateTime> RunCreatedDate
        {
            get;
            set;
        }
    }
}
