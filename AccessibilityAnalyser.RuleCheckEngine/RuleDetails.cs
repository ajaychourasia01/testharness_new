﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.RuleEngine
{
    public class RuleDetails
    {
        public string RulesDescription { get; set; }
        public string RuleName { get; set; }

        public int RuleID { get; set; }
        public int RuleDetailsID { get; set; }
        public int StandardID { get; set; }
        public int StandardTitle { get; set; }
    }
}
